# Finds Operating system
OS := $(shell uname -s)
NPROCS:= 1

# Compiler settings
CC = g++
CCFLAGS = -Wall -O3 -Werror -std=c++0x
DDFLAGS = -DDEBUG -DGL_GLEXT_PROTOTYPES -DGLEW_STATIC
AR = ar
ARFLAGS = rvs

# Folders
ROOT_DIR = .
SRC_NAME = source
OUT_NAME = bin
INC_NAME = include/Cupcake
FIND = $(shell find $(1) -type d -print)
EXCLUDE := $(call FIND, "./bin") $(call FIND, "./.git") \
	$(call FIND, "./.settings") $(call FIND, "./include")

# Include files
HDR_PATH = ../support/include
FT_PATH = /usr/include/freetype2
BULLET_PATH = /usr/local/include/bullet
INC_PATH := -Iinclude -I$(HDR_PATH) -I$(FT_PATH) -I$(BULLET_PATH)

# Library files
LDFLAGS = 
LIB_PATH := -L../support/lib

# CMD settings
CMD_MK_FOLDER = mkdir
CMD_FIND =

# Export settings
LIB_EXP = ../support/lib
HDR_EXP = ../support/include/Cupcake/.

# Linux
ifeq ($(OS),Linux)
	NPROCS:=$(shell grep -c ^processor /proc/cpuinfo)
	LDFLAGS += -lglfw3 -lGLEW -lGL -lfreetype -lz -lBulletSoftBody \
			   -lBulletDynamics -lBulletCollision -lLinearMath -l:libfmodex64.so
	CMD_FIND = find
	SRC_DIR := $(filter-out $(EXCLUDE), \
		$(shell find $(ROOT_DIR)/$(SRC_NAME) ! -name "src" -type d -print))
	OUT_DIR := $(subst $(SRC_NAME),$(OUT_NAME), $(SRC_DIR))
	SRC := $(shell find $(ROOT_DIR)/$(SRC_NAME) -name "*.cpp")
	HFILES := $(shell find $(ROOT_DIR)/$(SRC_NAME) -name "*.hpp")
endif
# OSX
ifeq ($(OS), OSX)
	NPROCS:=$(shell system_profiler | awk '/Number Of CPUs/{print $4}{next;}'}}})
endif

# Makeflags
MAKEFLAGS += -j$(NPROCS)

# Files
OBJ := $(subst $(SRC_NAME),$(OUT_NAME),$(patsubst %.cpp,%.o, $(SRC)))
H := $(subst $(SRC_NAME),$(INC_NAME), $(HFILES))

# Output settings
AR_NAME = $(OUT_NAME)/libcupcake.a

all:
	$(MAKE) $(OUT_DIR) make_header 
	$(MAKE) $(H)
	$(MAKE) export_hdr
	$(MAKE) $(AR_NAME)
	$(MAKE) export_lib

$(AR_NAME): $(OBJ)
	$(AR) $(ARFLAGS) $@ $(subst src,.,$^)

$(OUT_DIR) :
	@$(CMD_MK_FOLDER) -p $@

make_header :
	@$(CMD_MK_FOLDER) -p $(INC_NAME)

$(INC_NAME)/%.hpp : $(SRC_NAME)/%.hpp
	@cd $(SRC_NAME); cp --parent $(subst $(SRC_NAME),.,$<) ../$(INC_NAME); cd ..;

export_hdr :
	cp -r $(INC_NAME)/* $(HDR_EXP)

export_lib :
	cp $(AR_NAME) $(LIB_EXP);

$(OUT_NAME)/%.o : $(SRC_NAME)/%.cpp
	$(CC) -c -o $(subst src,.,$@) $< $(CCFLAGS) $(DDFLAGS) $(LDPATH) $(LDFLAGS) $(INC_PATH)

clean :
	-rm -rf $(OUT_NAME)
	-rm -rf $(INC_NAME)
	-rm -rf include
	-rm -rf $(HDR_EXP)/*
	-rm -rf $(LIB_EXP)/libcupcake.a
	@echo Clean done

debug:
	$(info $$SRC_DIR is $(SRC_DIR))
	$(info $$OUT_DIR is $(OUT_DIR))
	$(info $$SRC is $(SRC))
	$(info $$OBJ is $(OBJ))

.PHONY: all clean debug
