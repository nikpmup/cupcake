/*
 * Mat4Comp.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef MAT4COMP_HPP_
#define MAT4COMP_HPP_

// GL Libraries
#include <GL/glew.h>
// Math Libraries
#include <glm/glm.hpp>
// Physics Libraries
#include <bullet/btBulletDynamicsCommon.h>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Component containing 4x4 Matrix
 */
class Mat4Comp : public Component {
 public:
    /**
     * Assigns glm 4x4 matrix
     * @param mat4 : 4x4 matrix values
     */
    Mat4Comp(const glm::mat4 &mat4);
    /**
     * Assigns bullet 4x4 matrix
     * @param trans : 4x4 matrix values
     */
    Mat4Comp(const btTransform &trans);
    /**
     * Sets matrix as glm 4x4 matrix
     * @param trans : 4x4 matrix values
     */
    GLvoid setMatrix(const glm::mat4 &mat);
    /**
     * Sets matrix as bullet 4x4 matrix
     * @param trans : 4x4 matrix vallues
     */
    GLvoid setMatrix(const btTransform &trans);
    /**
     * Gets 4x4 matrix at a bullet 4x4 matrix
     * @return bullet 4x4 matrix
     */
    btTransform getBtMatrix();
    /**
     * Gets 4x4 matrix as a glm 4x4 matrix
     * @return glm 4x4 matrix
     */
    glm::mat4 getGlmMatrix();
    /**
     * 4x4 Matrix
     */
    btTransform mMatrix;
};


#endif /* MAT4COMP_HPP_ */
