/*
 * Mat4Comp.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Math/Mat4Comp.hpp>
// Math Libraries
#include <glm/gtc/type_ptr.hpp>

//----------------------------------------------------------------------------
//
Mat4Comp::Mat4Comp(const glm::mat4 &mat4) {
    setMatrix(mat4);
}

//----------------------------------------------------------------------------
//
Mat4Comp::Mat4Comp(const btTransform &trans) : mMatrix(trans) {
}

//----------------------------------------------------------------------------
//
GLvoid Mat4Comp::setMatrix(const glm::mat4 &mat) {
    mMatrix.setFromOpenGLMatrix(glm::value_ptr(mat));
}

//----------------------------------------------------------------------------
//
GLvoid Mat4Comp::setMatrix(const btTransform &trans) {
    mMatrix = trans;
}

//----------------------------------------------------------------------------
//
btTransform Mat4Comp::getBtMatrix() {
    return mMatrix;
}

//----------------------------------------------------------------------------
//
glm::mat4 Mat4Comp::getGlmMatrix() {
    glm::mat4 matrix;

    mMatrix.getOpenGLMatrix(glm::value_ptr(matrix));

    return matrix;
}
