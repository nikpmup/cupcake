/*
 * Vec3Comp.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Math/Vec3Comp.hpp>

//----------------------------------------------------------------------------
//
Vec3Comp::Vec3Comp(const GLfloat x, const GLfloat y, const GLfloat z)
        : mX(x),
          mY(y),
          mZ(z) {
}

//----------------------------------------------------------------------------
//
Vec3Comp::Vec3Comp(const glm::vec3 &pos)
        : mX(pos.x),
          mY(pos.y),
          mZ(pos.z) {
}

//----------------------------------------------------------------------------
//
Vec3Comp::Vec3Comp(const btVector3 &pos)
        : mX(pos.getX()),
          mY(pos.getY()),
          mZ(pos.getZ()) {
}

//----------------------------------------------------------------------------
//
Vec3Comp::Vec3Comp(const FMOD_VECTOR &pos)
        : mX(pos.x),
          mY(pos.y),
          mZ(pos.z) {
}

//----------------------------------------------------------------------------
//
GLvoid Vec3Comp::setVec(const GLfloat x, const GLfloat y, const GLfloat z) {
    mX = x;
    mY = y;
    mZ = z;
}

//----------------------------------------------------------------------------
//
GLvoid Vec3Comp::setVec(const glm::vec3 &pos) {
    mX = pos.x;
    mY = pos.y;
    mZ = pos.z;
}

//----------------------------------------------------------------------------
//
GLvoid Vec3Comp::setVec(const btVector3 &pos) {
    mX = pos.getX();
    mY = pos.getY();
    mZ = pos.getZ();
}

//----------------------------------------------------------------------------
//
GLvoid Vec3Comp::setVec(const FMOD_VECTOR &pos) {
    mX = pos.x;
    mY = pos.y;
    mZ = pos.z;
}

//----------------------------------------------------------------------------
//
glm::vec3 Vec3Comp::getGlmVec() {
    return glm::vec3(mX, mY, mZ);
}

//----------------------------------------------------------------------------
//
btVector3 Vec3Comp::getBtVec() {
    return btVector3(mX, mY, mZ);
}

//----------------------------------------------------------------------------
//
FMOD_VECTOR Vec3Comp::getFmodVec() {
    return (FMOD_VECTOR ) { mX, mY, mZ } ;
}
