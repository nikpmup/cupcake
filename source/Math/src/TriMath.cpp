/*
 * TriMath.cpp
 *
 *  Created on: Jan 16, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Math/TriMath.hpp>

glm::vec3 TriMath::calcNorm(const glm::vec3 v0, const glm::vec3 v1,
                                   const glm::vec3 v2) {
    return glm::normalize(glm::cross(v2 - v0, v1 - v0));
}

