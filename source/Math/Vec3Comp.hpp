/*
 * Vec3Comp.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef VEC3COMP_HPP_
#define VEC3COMP_HPP_

// GL Libraries
#include <GL/glew.h>
// Math Libraries
#include <glm/glm.hpp>
// Physics Libraries
#include <bullet/btBulletDynamicsCommon.h>
// Sound Libraries
#include <fmod/fmod.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Component containing 3D Vector
 */
class Vec3Comp : public Component {
 public:
    /**
     * Assigns x,y,z coordinates to the vector
     * @param x : X the vector in 3D space
     * @param y : Y the vector in 3D space
     * @param z : Z the vector in 3D space
     */
    Vec3Comp(const GLfloat x, const GLfloat y, const GLfloat z);
    /**
     * Assigns glm 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    Vec3Comp(const glm::vec3 &pos);
    /**
     * Assigns bullet 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    Vec3Comp(const btVector3 &pos);
    /**
     * Assigns FMOD 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    Vec3Comp(const FMOD_VECTOR &pos);
    /**
     * Assigns x,y,z coordinates to the vector
     * @param x : X the vector in 3D space
     * @param y : Y the vector in 3D space
     * @param z : Z the vector in 3D space
     */
    GLvoid setVec(const GLfloat x, const GLfloat y, const GLfloat z);
    /**
     * Assigns glm 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    GLvoid setVec(const glm::vec3 &pos);
    /**
     * Assigns bullet 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    GLvoid setVec(const btVector3 &pos);
    /**
     * Assigns FMOD 3D vector to the vector
     * @param pos : 3D vector the vector
     */
    GLvoid setVec(const FMOD_VECTOR &pos);
    /**
     * Retrieves the vector vector
     * @return glm 3D vector
     */
    glm::vec3 getGlmVec();
    /**
     * Retrieves the vector vector
     * @return bullet 3D vector
     */
    btVector3 getBtVec();
    /**
     * Retrieves the vector vector
     * @return FMOD 3D vector
     */
    FMOD_VECTOR getFmodVec();
    /**
     * X coordinate of vector
     */
    GLfloat mX;
    /**
     * Y coordinate of vector
     */
    GLfloat mY;
    /**
     * Z coordinate of vector
     */
    GLfloat mZ;
};

#endif /* VEC3COMP_HPP_ */
