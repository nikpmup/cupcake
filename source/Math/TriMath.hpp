/*
 * TriMath.hpp
 *
 *  Created on: Jan 16, 2014
 *      Author: nikpmup
 */

#ifndef TRIMATH_HPP_
#define TRIMATH_HPP_

// GL Libraries
#include <glm/glm.hpp>

class TriMath {

 public:
    static glm::vec3 calcNorm(const glm::vec3 v0, const glm::vec3 v1,
                              const glm::vec3 v2);
};

#endif /* TRIMATH_HPP_ */
