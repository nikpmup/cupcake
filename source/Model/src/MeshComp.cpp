/*
 * MeshComp.cpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Model/MeshComp.hpp>

//----------------------------------------------------------------------------
//
MeshComp::MeshComp() {

}

//----------------------------------------------------------------------------
//
MeshComp::MeshComp(const vVec3 &pos, const vVec2 &uv, const vVec3 &norm,
           const vGLuint &idx)
        : mPosVec(pos),
          mUvVec(uv),
          mNormVec(norm),
          mIdxVec(idx) {

}

//----------------------------------------------------------------------------
//
MeshComp::MeshComp(const MeshComp &mesh)
        : mPosVec(mesh.mPosVec),
          mUvVec(mesh.mUvVec),
          mNormVec(mesh.mNormVec),
          mIdxVec(mesh.mIdxVec) {
}

//----------------------------------------------------------------------------
//
GLfloat* MeshComp::getPosData() {
    return (GLfloat*)mPosVec.data();
}

//----------------------------------------------------------------------------
//
GLsizeiptr MeshComp::getPosSize() {
    return getVecSize<vVec3, glm::vec3>(&mPosVec);
}

//----------------------------------------------------------------------------
//
GLuint MeshComp::getPosCount() {
    return mPosVec.size();
}

//----------------------------------------------------------------------------
//
GLfloat* MeshComp::getUVData() {
    return (GLfloat*)mUvVec.data();
}

//----------------------------------------------------------------------------
//
GLsizeiptr MeshComp::getUVSize() {
    return getVecSize<vVec2, glm::vec2>(&mUvVec);
}

//----------------------------------------------------------------------------
//
GLuint MeshComp::getUVCount() {
    return mUvVec.size();
}

//----------------------------------------------------------------------------
//
GLfloat* MeshComp::getNormData() {
    return (GLfloat*)mNormVec.data();
}

//----------------------------------------------------------------------------
//
GLsizeiptr MeshComp::getNormSize() {
    return getVecSize<vVec3, glm::vec3>(&mNormVec);
}
//----------------------------------------------------------------------------
//
GLuint MeshComp::getNormCount() {
    return mNormVec.size();
}

//----------------------------------------------------------------------------
//
GLuint* MeshComp::getIdxData() {
    return mIdxVec.data();
}

//----------------------------------------------------------------------------
//
GLsizeiptr MeshComp::getIdxSize() {
    return getVecSize<vGLuint, GLuint>(&mIdxVec);
}

//----------------------------------------------------------------------------
//
GLuint MeshComp::getIdxCount() {
    return mIdxVec.size();
}

//----------------------------------------------------------------------------
//
template <class T, typename type>
GLsizeiptr MeshComp::getVecSize(T *vec) {
    return vec->size() * sizeof(type);
}
