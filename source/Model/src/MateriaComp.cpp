/*
 * MaterialComp.cpp
 *
 *  Created on: Jan 12, 2014
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Model/MaterialComp.hpp>

//----------------------------------------------------------------------------
//
MaterialComp::MaterialComp(const glm::vec3 ambient, const glm::vec3 diffuse,
                   const glm::vec3 specular, const GLfloat reflect)
        : mAmbient(ambient),
          mDiffuse(diffuse),
          mSpecular(specular),
          mReflect(reflect) {
}
