/*
 * MaterialComp.hpp
 *
 *  Created on: Jan 12, 2014
 *      Author: nikpmup
 */

#ifndef MATERIALCOMP_HPP_
#define MATERIALCOMP_HPP_

// GL Libraries
#include <glm/glm.hpp>
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Component containing the object material data
 */
class MaterialComp : public Component {

 public:
    /**
     * Constructs a material with the provided data
     * @param ambient : Ambient color
     * @param diffuse : Diffuse color
     * @param specular : Specular color
     * @param reflect : Reflective coefficient
     */
    MaterialComp(const glm::vec3 ambient, const glm::vec3 diffuse,
             const glm::vec3 specular, const GLfloat reflect);
    /**
     * Ambient color
     */
    glm::vec3 mAmbient;
    /**
     * Diffuse color
     */
    glm::vec3 mDiffuse;
    /**
     * Specular color
     */
    glm::vec3 mSpecular;
    /**
     * Reflective coefficient
     */
    GLfloat mReflect;

};

/**
 * List containing Material Components
 */
typedef Map<MaterialComp> MaterialList;

#endif /* MATERIALCOMP_HPP_ */
