/*
 * TextureComp.hpp
 *
 *  Created on: Feb 23, 2014
 *      Author: nikpmup
 */

#ifndef TEXTURECOMP_HPP_
#define TEXTURECOMP_HPP_

// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

class TextureComp : public Component {
 public:
    TextureComp(const GLuint texID) : mTexID(texID) {}
    ~TextureComp() {
        glDeleteTextures(1, &mTexID);
    }

    GLuint mTexID;
};

typedef Map<TextureComp> TextureList;

#endif /* TEXTURECOMP_HPP_ */
