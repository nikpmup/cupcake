/*
 * MeshComp.hpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

#ifndef MESH_HPP_
#define MESH_HPP_

// STD Libraries
#include <vector>
// GL Libraries
#include <GL/glew.h>
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

class MeshComp : public Component {
 public:
    // Type definitions
    typedef std::vector<glm::vec3> vVec3;
    typedef std::vector<glm::vec2> vVec2;
    typedef std::vector<GLuint> vGLuint;

    // Constructors
    MeshComp();
    MeshComp(const vVec3 &pos, const vVec2 &uv, const vVec3 &norm,
         const vGLuint &idx);
    MeshComp(const MeshComp &mesh);

    // Position Data
    GLfloat* getPosData();
    GLsizeiptr getPosSize();
    GLuint getPosCount();

    // UV Data
    GLfloat* getUVData();
    GLsizeiptr getUVSize();
    GLuint getUVCount();

    // Normal Data
    GLfloat* getNormData();
    GLsizeiptr getNormSize();
    GLuint getNormCount();

    // Index Data
    GLuint* getIdxData();
    GLsizeiptr getIdxSize();
    GLuint getIdxCount();

    vVec3 mPosVec;
    vVec2 mUvVec;
    vVec3 mNormVec;
    vGLuint mIdxVec;
 private:
    // Private variables
    // Vector helper methods
    template <class T, typename type>
    GLsizeiptr getVecSize(T *vec);
};

typedef Map<MeshComp> MeshList;

#endif /* MESH_HPP_ */
