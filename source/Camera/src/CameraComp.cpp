/*
 * CameraComp.cpp
 *
 *  Created on: Jan 12, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Camera/CameraComp.hpp>
// GL Libraries
#include <glm/gtc/matrix_transform.hpp>

//----------------------------------------------------------------------------
//
CameraComp::CameraComp() {
}

//----------------------------------------------------------------------------
//
CameraComp::~CameraComp() {
}

//----------------------------------------------------------------------------
//
GLvoid CameraComp::setProjMat(const GLfloat fov, const GLfloat ratio,
                          const GLfloat zNear, const GLfloat zFar) {
    mProjMat = glm::perspective(fov, ratio, zNear, zFar);
}

//----------------------------------------------------------------------------
//
glm::mat4 CameraComp::getProjMat() {
    return mProjMat;
}

//----------------------------------------------------------------------------
//
GLvoid CameraComp::setViewMat(const glm::vec3 &pos, const glm::vec3 &lookAt,
                          const glm::vec3 &up) {
    mPos = pos;
    mLookAt = lookAt;
    mUp = up;
}

//----------------------------------------------------------------------------
//
glm::mat4 CameraComp::getViewMat() {
    const glm::mat4 viewMat = glm::lookAt(mPos, mLookAt, mUp);

    return viewMat;
}

//----------------------------------------------------------------------------
//
glm::vec3 CameraComp::getPos() {
    return mPos;
}
