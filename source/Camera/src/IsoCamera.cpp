/*
 * IsoCamera.cpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Camera/IsoCamera.hpp>
// STD Libraries
#include <cmath>
// GL Libraries
#include <glm/gtc/matrix_transform.hpp>

// 1 unit away from origin in xyz
#define DIST 0.57735026919

//----------------------------------------------------------------------------
// Initializes camera view to 1 unit away from origin
//----------------------------------------------------------------------------
//
IsoCamera::IsoCamera() {
    setViewMat(glm::vec3(DIST, DIST, DIST), glm::vec3(0.0f, 0.0f, 0.0f),
               glm::vec3(0.0f, 1.0f, 0.0f));
}

//----------------------------------------------------------------------------
//
GLvoid IsoCamera::setProjMat(const GLfloat left, const GLfloat right,
                             const GLfloat bottom, const GLfloat top,
                             const GLfloat zNear, const GLfloat zFar) {
    mProjMat = glm::ortho(left, right, bottom, top, zNear, zFar);
}
