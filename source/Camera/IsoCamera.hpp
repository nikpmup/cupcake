/*
 * IsoCamera.hpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

#ifndef ISOCAMERA_HPP_
#define ISOCAMERA_HPP_

// GL Libraries
#include <GL/glew.h>
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Camera/CameraComp.hpp>

class IsoCamera : public CameraComp {
 public:
    // Constructor
    IsoCamera();
    // Projection Matrix
    virtual GLvoid setProjMat(const GLfloat left, const GLfloat right,
                              const GLfloat bottom, const GLfloat top,
                              const GLfloat zNear, const GLfloat zFar);
};

#endif /* ISOCAMERA_HPP_ */
