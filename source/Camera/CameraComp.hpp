/*
 * CameraComp.hpp
 *
 *  Created on: Jan 12, 2014
 *      Author: nikpmup
 */

#ifndef CAMERACOMP_HPP_
#define CAMERACOMP_HPP_

// GL Libraries
#include <GL/glew.h>
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

class CameraComp : public Component {

 public:
    // Constructor
    CameraComp();
    virtual ~CameraComp();
    // Projection Matrix
    virtual GLvoid setProjMat(const GLfloat fov, const GLfloat ratio,
                      const GLfloat zNear, const GLfloat zFar);
    glm::mat4 getProjMat();
    // View Matrix
    GLvoid setViewMat(const glm::vec3 &pos, const glm::vec3 &lookAt,
                      const glm::vec3 &up);
    glm::mat4 getViewMat();
    // Position
    glm::vec3 getPos();

 protected:
    // Projection Members
    glm::mat4 mProjMat;
    // View Matrix Members
    glm::vec3 mPos;
    glm::vec3 mLookAt;
    glm::vec3 mUp;
};

typedef Map<CameraComp> CameraList;

#endif /* CAMERACOMP_HPP_ */
