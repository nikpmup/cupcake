/*
 * ScaleComp.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef SCALECOMP_HPP_
#define SCALECOMP_HPP_

// Cupcake Libraries
#include <Cupcake/Math/Vec3Comp.hpp>

/**
 * Component containing 3D Vector representing Scale
 */
class ScaleComp : public Vec3Comp {
 public:
    ScaleComp(const glm::vec3 &scale) : Vec3Comp(scale) {}
};

typedef Map<ScaleComp> ScaleList;

#endif /* SCALECOMP_HPP_ */
