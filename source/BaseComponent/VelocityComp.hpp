/*
 * VelocityComp.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef VELOCITYCOMP_HPP_
#define VELOCITYCOMP_HPP_

// Cupcake Libraries
#include <Cupcake/Math/Vec3Comp.hpp>

/**
 * Component with 3D vector representing velocity
 */
class VelocityComp : public Vec3Comp {
};

typedef Map<VelocityComp> VelocityList;

#endif /* VELOCITYCOMP_HPP_ */
