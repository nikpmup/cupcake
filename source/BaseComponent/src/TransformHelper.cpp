/*
 * TransformHelper.cpp
 *
 *  Created on: Feb 26, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/BaseComponent/TransformHelper.hpp>
// Math Library
#include <glm/gtx/transform.hpp>
// Cupcake Libraries
#include <Cupcake/Util/TypeConverter.hpp>

//----------------------------------------------------------------------------
//
TransformComp* TransformHelper::createTrans(const glm::vec3 &pos,
                                      const glm::mat4 &rot) {
    const glm::mat4 transform = glm::translate(pos) * rot;

    return new TransformComp(transform);
}

//----------------------------------------------------------------------------
//
GLvoid TransformHelper::setPos(TransformComp *trans, const glm::vec3 &pos) {
    trans->mMatrix.setOrigin(TypeConverter::glmToBT(pos));
}

//----------------------------------------------------------------------------
//
GLvoid TransformHelper::setPos(TransformComp *trans, const btVector3 &pos) {
    trans->mMatrix.setOrigin(pos);
}

//----------------------------------------------------------------------------
//
glm::vec3 TransformHelper::getGlmPos(TransformComp *trans) {
    return TypeConverter::BTToglm(trans->mMatrix.getOrigin());
}

//----------------------------------------------------------------------------
//
btVector3 TransformHelper::getBtPos(TransformComp *trans) {
    return trans->mMatrix.getOrigin();
}

//----------------------------------------------------------------------------
//
FMOD_VECTOR TransformHelper::getFmodPos(TransformComp *trans) {
    return TypeConverter::btToFmod(trans->mMatrix.getOrigin());
}

//----------------------------------------------------------------------------
//
FMOD_VECTOR TransformHelper::getFmodForward(TransformComp *trans) {
    return TypeConverter::btToFmod(trans->mMatrix.getBasis().getColumn(2));
}

//----------------------------------------------------------------------------
//
FMOD_VECTOR TransformHelper::getFmodUp(TransformComp *trans) {
    return TypeConverter::btToFmod(trans->mMatrix.getBasis().getColumn(1));
}
