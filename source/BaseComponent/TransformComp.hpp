/*
 * TransformComp.hpp
 *
 *  Created on: Feb 26, 2014
 *      Author: nikpmup
 */

#ifndef TRANSFORMCOMP_HPP_
#define TRANSFORMCOMP_HPP_

// Cupcake Libraries
#include <Cupcake/Math/Mat4Comp.hpp>

/**
 * Transformation matrix component
 */
class TransformComp : public Mat4Comp {

 public:
    /**
     * Initializes transformation with identity
     */
    TransformComp() : Mat4Comp(glm::mat4(1.0f)) {}
    /**
     * Initializes transformation with glm matrix
     * @param mat : Glm matrix
     */
    TransformComp(const glm::mat4 &mat) : Mat4Comp(mat) {}
    /**
     * Initializes transformation with bullet matrix
     * @param trans : Bullet matrix
     */
    TransformComp(const btTransform &trans) : Mat4Comp(trans) {}
};

typedef Map<TransformComp> TransfList;

#endif /* TRANSFORMCOMP_HPP_ */
