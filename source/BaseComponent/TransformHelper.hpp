/*
 * TransformHelper.hpp
 *
 *  Created on: Feb 26, 2014
 *      Author: nikpmup
 */

#ifndef TRANSFORMHELPER_HPP_
#define TRANSFORMHELPER_HPP_

// Cupcake Libraries
#include <Cupcake/BaseComponent/TransformComp.hpp>
// Sound Libraries
#include <fmod/fmod.hpp>

/**
 * Helper class to alter the tranformation component
 */
class TransformHelper {
 public:
    /**
     * Creates a Transformation component from position, scale, and rotation
     * @param pos : GLM position vector
     * @param rot : GLM rotation matrix
     * @return Pointer to new Transformation component
     */
    static TransformComp* createTrans(const glm::vec3 &pos,
                                      const glm::mat4 &rot);
    /**
     * Sets the transformation position
     * @param trans : Transform component to alter
     * @param pos : GLM position
     */
    static GLvoid setPos(TransformComp *trans, const glm::vec3 &pos);
    /**
     * Sets the transformation position
     * @param trans : Transform component to alter
     * @param pos : Bullet position
     */
    static GLvoid setPos(TransformComp *trans, const btVector3 &pos);
    /**
     * Retrieves position
     * @param trans : Transform component to get position from
     * @return Glm vector of position
     */
    static glm::vec3 getGlmPos(TransformComp *trans);
    /**
     * Retrieves position
     * @param trans : Transform component to get position from
     * @return Bullet vector of position
     */
    static btVector3 getBtPos(TransformComp *trans);
    /**
     * Retrieves position
     * @param trans : Transform component to get position from
     * @return FMOD vector of position
     */
    static FMOD_VECTOR getFmodPos(TransformComp *trans);
    // TODO : Add rotation matrix and scale retrieval
    /**
     * Retrieves forward vector
     * @param trans : Transform component to get forward vector from
     * @return FMOD forward vector
     */
    static FMOD_VECTOR getFmodForward(TransformComp *trans);
    /**
     * Retrieves up vector
     * @param trans : Transform component to get up vector from
     * @return FMOD up vector
     */
    static FMOD_VECTOR getFmodUp(TransformComp *trans);
};

#endif /* TRANSFORMHELPER_HPP_ */
