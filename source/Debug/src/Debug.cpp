/*
 * Debug.cpp
 *
 *  Created on: Jan 7, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Debug/Debug.hpp>
// STD Libraries
#include <cstdio>
#include <cstdlib>

//----------------------------------------------------------------------------
//
GLboolean printGLError(const GLchar *str) {
    GLenum glErr = glGetError();
    const GLboolean isError = glErr != GL_NO_ERROR;

    if (isError) {
        fprintf(stderr, "glError %s: %s\n", str, gluErrorString(glErr));
    }

    return isError;
}

//----------------------------------------------------------------------------
//
GLvoid printAudioError(const FMOD_RESULT result) {
    if (result != FMOD_OK) {
        printError("SoundManager: (%d) %s\n", result,
                   FMOD_ErrorString(result));
        exit(EXIT_FAILURE);
    }
}

//----------------------------------------------------------------------------
//
GLvoid printError(const GLchar *message, ...) {
    va_list args;
    va_start(args, message);

    vfprintf(stderr, message, args);

    va_end(args);
}
