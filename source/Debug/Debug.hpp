/*
 * Debug.hpp
 *
 *  Created on: Jan 7, 2014
 *      Author: nikpmup
 */

#ifndef DEBUG_HPP_
#define DEBUG_HPP_

// GL Libraries
#include <GL/glew.h>
// Sound Libraries
#include <fmod/fmod.h>
#include <fmod/fmod_errors.h>
// STD Libraries
#include <cstdarg>

#ifdef DEBUG
#define DBG_CMD(x) x
#else
#define DBG_CMD(x)
#endif

// GL Error check
GLboolean printGLError(const GLchar *str);
// Audio Error Check
GLvoid printAudioError(const FMOD_RESULT result);
// Print to error file
GLvoid printError(const GLchar *msg, ...);

#endif /* DEBUG_HPP_ */
