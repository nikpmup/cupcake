/*
 * Factory.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef FACTORY_HPP_
#define FACTORY_HPP_

// STD Libraries
#include <vector>

template <typename T>
class Factory {

 protected:
    Factory(T &sys) : mSystem(sys) {}

    T &mSystem;
};


#endif /* FACTORY_HPP_ */
