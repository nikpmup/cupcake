/*
 * Map.hpp
 *
 *  Created on: Feb 4, 2014
 *      Author: nikpmup
 */

#ifndef MAP_HPP_
#define MAP_HPP_

// STD Libraries
#include <map>
// GL Libraries
#include <GL/glew.h>

template<typename T>
class Map {

 public:
    /**
     * Deletes entry from map and removes value from memory
     * @param id : ID number of the entry
     */
    inline GLvoid releaseEntry(const GLuint id) {
        delete mMap[id];
        mMap.erase(id);
    }

    /**
     * Deletes every entry from the map and removes the values from memory
     */
    inline GLvoid releaseAll() {
        auto itr = mMap.begin();
        while (itr != mMap.end()) {
            delete itr->second;
            itr = mMap.erase(itr);
        }
    }

    /**
     * Retrieves entry at specified ID
     * @param id : Entry ID
     */
    inline T* getEntry(const GLuint id) {
        return mMap.at(id);
    }

    /**
     * Adds entry to map at specified ID
     * @param id : Entry ID
     * @param entiry : Entry to add
     */
    inline GLvoid addEntry(const GLuint id, T* entry) {
        mMap[id] = entry;
    }

    /**
     * Check if key exists
     * @param id : Key value
     * @return True if value exists
     */
    inline GLboolean hasEntry(const GLuint id) {
        return mMap.find(id) != mMap.end();
    }
    /**
     * Typdef of map value
     */
    typedef std::map<GLuint, T*> tMap;
    /**
     * Map value
     */
    tMap mMap;

};


#endif /* MAP_HPP_ */
