/*
 * System.hpp
 *
 *  Created on: Feb 18, 2014
 *      Author: nikpmup
 */

#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

// GL Libraries
#include <GL/glew.h>

/**
 * Interface for Systems
 */
class System {
 public:
    /**
     * Deconstructor of the system
     */
    virtual ~System() {}
    /**
     * Updates the system
     */
    virtual GLvoid update() = 0;
    /**
     * Initializes the system
     */
    virtual GLvoid init() = 0;
    /**
     * Releases assets in the system
     */
    virtual GLvoid release() = 0;
};

#endif /* SYSTEM_HPP_ */
