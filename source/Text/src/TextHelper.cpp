/*
 * TextHelper.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Text/TextHelper.hpp>
// STD Libraries
#include <cstring>
// Cupcake Libraries
#include <Cupcake/Math/TriMath.hpp>

//----------------------------------------------------------------------------
//
MeshComp* TextHelper::createText(AtlasComp *atlas, const GLchar *text,
                                 glm::vec2 pos, const glm::vec2 &scale) {
    const GLuint idxSize = 6 * strlen(text);

    MeshComp::vVec3 posData(idxSize);
    MeshComp::vVec3 normData(idxSize);
    MeshComp::vVec2 coordData(idxSize);
    MeshComp::vGLuint idxData(idxSize);

    GLuint idx = 0;

    for (const GLubyte *p = (const GLubyte*) text; *p; p++) {
        AtlasComp::CharInfo &info = atlas->mCharInfo[*p];
        GLfloat x2 = pos.x + info.btSize.x * scale.x;
        GLfloat y2 = -pos.y - info.btSize.y * scale.y;
        GLfloat w = info.btBound.x * scale.x;
        GLfloat h = info.btBound.y * scale.y;

        pos.x += info.adv.x * scale.x;
        pos.y += info.adv.y * scale.y;

        if (!w || !h)
            continue;

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2, -y2 - h, 0.0f);
        coordData[idx++] = glm::vec2(
                info.texOff.x,
                info.texOff.y + info.btBound.y / atlas->mHeightTex);

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2 + w, -y2, 0.0f);
        coordData[idx++] = glm::vec2(
                info.texOff.x + info.btBound.x / atlas->mWidthTex,
                info.texOff.y);

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2, -y2, 0.0f);
        coordData[idx++] = glm::vec2(info.texOff.x, info.texOff.y);

        glm::vec3 norm = TriMath::calcNorm(posData[idx - 3], posData[idx - 2],
                                           posData[idx - 1]);
        normData[idx - 3] = norm;
        normData[idx - 2] = norm;
        normData[idx - 2] = norm;

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2 + w, -y2, 0.0f);
        coordData[idx++] = glm::vec2(
                info.texOff.x + info.btBound.x / atlas->mWidthTex,
                info.texOff.y);

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2, -y2 - h, 0.0f);
        coordData[idx++] = glm::vec2(
                info.texOff.x,
                info.texOff.y + info.btBound.y / atlas->mHeightTex);

        idxData[idx] = idx;
        posData[idx] = glm::vec3(x2 + w, -y2 - h, 0.0f);
        coordData[idx++] = glm::vec2(
                info.texOff.x + info.btBound.x / atlas->mWidthTex,
                info.texOff.y + info.btBound.y / atlas->mHeightTex);

        norm = TriMath::calcNorm(posData[idx - 3], posData[idx - 2],
                                 posData[idx - 1]);
        normData[idx - 3] = norm;
        normData[idx - 2] = norm;
        normData[idx - 2] = norm;
    }

    return new MeshComp(posData, coordData, normData, idxData);
}
