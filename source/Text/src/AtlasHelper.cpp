/*
 * AtlasHelper.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Text/AtlasHelper.hpp>
// STD Libraries
#include <cstdlib>
#include <algorithm>
// Math Libraries
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Debug/Debug.hpp>

#define MAXWIDTH 1024       // Max texture size
#define ASCII_MIN 32
#define ASCII_MAX 128

//----------------------------------------------------------------------------
//
AtlasComp* AtlasHelper::createAtlas(FT_Face face) {
    AtlasComp *atlas = new AtlasComp();

    calcSize(atlas, face);
    createTex(atlas, face);

    return atlas;
}

//----------------------------------------------------------------------------
//
GLvoid AtlasHelper::calcSize(AtlasComp *atlas, FT_Face face) {
    GLint rowW = 0;
    GLint rowH = 0;
    FT_GlyphSlot g = face->glyph;

    for (GLint cIdx = ASCII_MIN; cIdx < ASCII_MAX; cIdx++) {
        if (FT_Load_Char(face, cIdx, FT_LOAD_RENDER)) {
            printError("AtlasHelper: Loading character failed\n");
            exit(EXIT_FAILURE);
        }
        if (rowW + g->bitmap.width + 1 >= MAXWIDTH) {
            atlas->mWidthTex = std::max(atlas->mWidthTex, rowW);
            atlas->mHeightTex += rowH;
            rowW = 0;
            rowH = 0;
        }
        rowW += g->bitmap.width + 1;
        rowH = std::max(rowH, g->bitmap.rows);
    }

    atlas->mWidthTex = std::max(atlas->mWidthTex, rowW);
    atlas->mHeightTex += rowH;
}

//----------------------------------------------------------------------------
//
GLvoid AtlasHelper::createTex(AtlasComp *atlas, FT_Face face) {
    FT_GlyphSlot glyph = face->glyph;

    if (glyph == NULL) {
        printError("AtlasHelper: Invalid glyph\n");
        exit(EXIT_FAILURE);
    }
    /* Create a texture that will be used to hold all ASCII glyphs */
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &atlas->mTexId);
    glBindTexture(GL_TEXTURE_2D, atlas->mTexId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, atlas->mWidthTex, atlas->mHeightTex,
                 0, GL_RED,
                 GL_UNSIGNED_BYTE,
                 0);
    /* We require 1 byte alignment when uploading texture data */
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    /* Clamping to edges is important to prevent artifacts when scaling */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    /* Linear filtering usually looks best for text */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLint offX = 0;
    GLint offY = 0;
    GLint rowH = 0;

    for (GLint cIdx = ASCII_MIN; cIdx < ASCII_MAX; cIdx++) {
        if (FT_Load_Char(face, cIdx, FT_LOAD_RENDER)) {
            printError("AtlasHelper: Loading character failed\n");
            exit(EXIT_FAILURE);
        }

        if (offX + glyph->bitmap.width + 1 >= MAXWIDTH) {
            offY += rowH;
            rowH = 0;
            offX = 0;
        }

        glTexSubImage2D(GL_TEXTURE_2D, 0, offX, offY, glyph->bitmap.width,
                        glyph->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE,
                        glyph->bitmap.buffer);

        atlas->mCharInfo[cIdx].adv.x = glyph->advance.x >> 6;
        atlas->mCharInfo[cIdx].adv.y = glyph->advance.y >> 6;

        atlas->mCharInfo[cIdx].btBound.x = glyph->bitmap.width;
        atlas->mCharInfo[cIdx].btBound.y = glyph->bitmap.rows;

        atlas->mCharInfo[cIdx].btSize.x = glyph->bitmap_left;
        atlas->mCharInfo[cIdx].btSize.y = glyph->bitmap_top;

        atlas->mCharInfo[cIdx].texOff.x = offX / (GLfloat) atlas->mWidthTex;
        atlas->mCharInfo[cIdx].texOff.y = offY / (GLfloat) atlas->mHeightTex;

        rowH = std::max(rowH, glyph->bitmap.rows);
        offX += glyph->bitmap.width + 1;
    }

    DBG_CMD(printf("Generated a %d x %d (%d kb) texture atlas\n", atlas->mWidthTex,
                    atlas->mHeightTex, atlas->mWidthTex * atlas->mHeightTex / 1024));
}
