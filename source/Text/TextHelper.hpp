/*
 * TextHelper.hpp
 *
 *  Created on: Feb 27, 2014
 *      Author: nikpmup
 */

#ifndef TEXTHELPER_HPP_
#define TEXTHELPER_HPP_

// Cupcake Libraries
#include <Cupcake/Text/AtlasComp.hpp>
#include <Cupcake/Model/MeshComp.hpp>

class TextHelper {
 public:
    static MeshComp* createText(AtlasComp *atlas, const GLchar *text,
                                glm::vec2 pos, const glm::vec2 &scale);
};

#endif /* TEXTHELPER_HPP_ */
