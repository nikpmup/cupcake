/*
 * AtlasHelper.hpp
 *
 *  Created on: Feb 27, 2014
 *      Author: nikpmup
 */

#ifndef ATLASHELPER_HPP_
#define ATLASHELPER_HPP_

// Cupcake Libraries
#include <Cupcake/Text/AtlasComp.hpp>
// Font Libraries
#include <ft2build.h>
#include FT_FREETYPE_H

/**
 * Helper class that creates Atlas Components
 */
class AtlasHelper {
 public:
    /**
     * Creates an atlas component with the given face
     * @param face : Face to generate atlas off of
     * @see Render2DSystem
     */
    static AtlasComp* createAtlas(FT_Face face);

 private:
    /**
     * Calculates size of atlas texture using face
     * @param atlas : Atlas to assign size to
     * @param face : Face to calculate texture size
     */
    static GLvoid calcSize(AtlasComp *atlas, FT_Face face);
    /**
     * Creates texture off of face
     * @param atlas : Atlas to assign texture to
     * @param face : Face to build texture off of
     */
    static GLvoid createTex(AtlasComp *atlas, FT_Face face);
};



#endif /* ATLASHELPER_HPP_ */
