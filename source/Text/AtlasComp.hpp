/*
 * AtlasComp.hpp
 *
 *  Created on: Feb 26, 2014
 *      Author: nikpmup
 */

#ifndef ATLASCOMP_HPP_
#define ATLASCOMP_HPP_

// GL Libraries
#include <GL/glew.h>
// Math Libraries
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Component containing an atlas of a font
 */
class AtlasComp : public Component {

 public:
    /**
     * Contains information for each character
     */
    typedef struct CharInfo {
        glm::vec2 adv;  /**< Cursor advancement for next char */
        glm::vec2 btSize; /**< Bitmap width and height */
        glm::vec2 btBound; /**< Bitmap top and left */
        glm::vec2 texOff; /**< Glyph offset in texture coord*/
    } CharInfo;

    /**
     * Creates empty atlas component
     */
    AtlasComp() : mTexId(0), mWidthTex(0), mHeightTex(0), mCharInfo() {}
    /**
     * Deletes texture if created
     */
    ~AtlasComp() {
        if (mTexId != 0)
            glDeleteTextures(1, &mTexId);
    }
    /**
     * Texture Id of atlas
     */
    GLuint mTexId;
    /**
     * Width of texture
     */
    GLint mWidthTex;
    /**
     * Height of texture
     */
    GLint mHeightTex;

    CharInfo mCharInfo[128];
};

typedef Map<AtlasComp> AtlasList;

#endif /* ATLASCOMP_HPP_ */
