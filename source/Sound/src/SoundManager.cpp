/*
 * SoundManager.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: nikpmup
 */

// Header definition
// Sound Libraries
//#include <fmod/fmod_errors.h>
//// STD Libraries
//#include <cstdlib>
//// Cupcake Libraries
//#include <Cupcake/Debug/Debug.hpp>
//#include <Cupcake/Util/TypeConverter.hpp>

////----------------------------------------------------------------------------
////
//SoundManager::SoundManager()
//        : mSystem(NULL) {
//    initSystem();
//}
//
////----------------------------------------------------------------------------
////
//SoundManager::~SoundManager() {
//    releaseAllSound();
//    mSystem->close();
//    mSystem->release();
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::createSound(const GLuint tag, const GLchar *file) {
//    addEntry(tag, createSound(file));
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::createStream(const GLuint tag, const GLchar *file) {
//    addEntry(tag, createStream(file));
//}
//
////----------------------------------------------------------------------------
////
//Sound* SoundManager::createSound(const GLchar *file) {
//    Sound *sound = new Sound();
//    FMOD_RESULT result;
//    if (!mSystem) {
//        printError("SoundManager: System not initialized\n");
//    }
//    result = mSystem->createSound(file, FMOD_SOFTWARE | FMOD_3D, 0,
//                                  sound->getSound());
//    printAudioError(result);
//
//    return sound;
//}
//
////----------------------------------------------------------------------------
////
//Sound* SoundManager::createStream(const GLchar *file) {
//    Sound *sound = new Sound();
//    FMOD_RESULT result;
//    if (!mSystem) {
//        printError("SoundManager: System not initialized\n");
//    }
//    result = mSystem->createStream(file, FMOD_SOFTWARE | FMOD_3D, 0,
//                                   sound->getSound());
//    printAudioError(result);
//
//    return sound;
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::setDistance(const GLuint tag, const GLfloat min,
//                                 const GLfloat max) {
//    (*getEntry(tag)->getSound())->set3DMinMaxDistance(min, max);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::setSoundVolume(const GLuint tag, const GLfloat volume) {
//    (*getEntry(tag)->getChannel())->setVolume(volume);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::playSound(const GLuint tag) {
//    Sound *sound = getEntry(tag);
//    FMOD_RESULT result;
//    if (!mSystem) {
//        printError("SoundManager: System not initialized\n");
//    }
//    result = mSystem->playSound(FMOD_CHANNEL_FREE, *sound->getSound(), false,
//                                sound->getChannel());
//    printAudioError(result);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::playSound(const GLuint tag, const glm::vec3 pos,
//                               const glm::vec3 vel) {
//    Sound *sound = getEntry(tag);
//    FMOD_RESULT result;
//
//    if (!mSystem) {
//        printError("SoundManager: System not initialized\n");
//    }
//    result = mSystem->playSound(FMOD_CHANNEL_FREE, *sound->getSound(), true,
//                                sound->getChannel());
//    printAudioError(result);
//    setSoundLocation(tag, pos, vel);
//    printAudioError(result);
//    result = (*sound->getChannel())->setPaused(false);
//    printAudioError(result);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::setSoundMode(const GLuint tag, const FMOD_MODE mode) {
//    (*getEntry(tag)->getSound())->setMode(mode);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::setSoundLocation(const GLuint tag, const glm::vec3 pos,
//                                      const glm::vec3 vel) {
//    const FMOD_VECTOR fpos = TypeConverter::glmToFMOD(pos);
//    const FMOD_VECTOR fvel = TypeConverter::glmToFMOD(vel);
//
//    (*getEntry(tag)->getChannel())->set3DAttributes(&fpos, &fvel);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::initSystem() {
//    const GLint max_channel = 10;
//    FMOD_RESULT result;
//
//    result = FMOD::System_Create(&mSystem);
//    printAudioError(result);
//    result = mSystem->init(max_channel, FMOD_INIT_NORMAL, 0);
//    printAudioError(result);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::releaseSound(const GLuint tag) {
//    releaseEntry(tag);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::releaseAllSound() {
//    releaseAll();
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::attachListener(SoundListener *list) {
//    mListener = list;
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::releaseListener() {
//    mListener = NULL;
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::updateListener() {
//    FMOD_RESULT result;
//    result = mSystem->set3DListenerAttributes(0, &mListener->mPos,
//                                              &mListener->mVel,
//                                              &mListener->mForward,
//                                              &mListener->mUp);
//    printAudioError(result);
//}
//
////----------------------------------------------------------------------------
////
//GLvoid SoundManager::update() {
//    if (!mSystem) {
//        printError("SoundManager: System has not been initialized\n");
//    }
//    mSystem->update();
//}
