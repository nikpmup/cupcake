/*
 * SoundSystem.cpp
 *
 *  Created on: Mar 5, 2014
 *      Author: nikpmup
 */

// Header Functions
#include <Cupcake/Sound/SoundSystem.hpp>
// Cupcake Libraries
#include <Cupcake/BaseComponent/TransformHelper.hpp>
#include <Cupcake/Debug/Debug.hpp>

//----------------------------------------------------------------------------
//
SoundSystem::SoundSystem(TransfList &transf)
        : mTransfList(transf),
          mListenerID(0),
          mSystem(NULL) {
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::update() {
    for (auto iter = mSoundList.mMap.begin(); iter != mSoundList.mMap.end();
            ++iter) {
        updatePosition(iter->first);
    }
    updateListener(mListenerID);

    mSystem->update();
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::init() {
    initSystem();
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::initSystem() {
    const GLint max_channel = 10;
    FMOD_RESULT result;

    result = FMOD::System_Create(&mSystem);
    printAudioError(result);
    result = mSystem->init(max_channel, FMOD_INIT_NORMAL, 0);
    printAudioError(result);
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::release() {
    mSoundList.releaseAll();
    mSystem->close();
    mSystem->release();
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::addSound(Entity &id, const GLchar *soundFile) {
    SoundComp *sound = new SoundComp();
    FMOD_RESULT result;

    result = mSystem->createSound(soundFile, FMOD_SOFTWARE | FMOD_3D, 0,
                                  &sound->mSound);
    printAudioError(result);

    mSoundList.addEntry(id, sound);
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::addStream(Entity &id, const GLchar *soundFile) {
    SoundComp *sound = new SoundComp();
    FMOD_RESULT result;

    result = mSystem->createStream(soundFile, FMOD_SOFTWARE | FMOD_3D, 0,
                                   &sound->mSound);
    printAudioError(result);

    mSoundList.addEntry(id, sound);
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::playSound(Entity &id, const FMOD_MODE &mode,
                              const GLfloat &min, const GLfloat &max,
                              const GLfloat &volume) {
    SoundComp *sound = mSoundList.getEntry(id);
    FMOD_RESULT result;

    result = sound->mSound->setMode(mode);
    printAudioError(result);
    result = mSystem->playSound(FMOD_CHANNEL_FREE, sound->mSound, true,
                                &sound->mChannel);
    printAudioError(result);
    result = sound->mChannel->set3DMinMaxDistance(min, max);
    printAudioError(result);
    updatePosition(id);
    result = sound->mChannel->setPaused(false);
    printAudioError(result);
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::updatePosition(const Entity &id) {
    SoundComp *sound = mSoundList.getEntry(id);
    TransformComp *trans = mTransfList.getEntry(id);
    FMOD_RESULT result;

    FMOD_VECTOR pos = TransformHelper::getFmodPos(trans);
    FMOD_VECTOR vel = (FMOD_VECTOR ) { 0, 0, 0 };

    result = sound->mChannel->set3DAttributes(&pos, &vel);
    printAudioError(result);
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::addListener(const Entity &id) {
    mListenerID = id;
}

//----------------------------------------------------------------------------
//
GLvoid SoundSystem::updateListener(const Entity &id) {
    TransformComp *trans = mTransfList.getEntry(id);
    FMOD_RESULT result;

    FMOD_VECTOR pos = TransformHelper::getFmodPos(trans);
    FMOD_VECTOR up = TransformHelper::getFmodUp(trans);
    FMOD_VECTOR forward = TransformHelper::getFmodForward(trans);
    FMOD_VECTOR vel = (FMOD_VECTOR ) { 0, 0, 0 };  // TODO : Add Velocity

    result = mSystem->set3DListenerAttributes(0, &pos, &vel, &forward, &up);
    printAudioError(result);
}
