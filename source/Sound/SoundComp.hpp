/*
 * SoundComp.hpp
 *
 *  Created on: Mar 5, 2014
 *      Author: nikpmup
 */

#ifndef SOUNDCOMP_HPP_
#define SOUNDCOMP_HPP_

// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>
// Sound Libraries
#include <fmod/fmod.hpp>

/**
 * Sound Component containing FMOD data
 */
class SoundComp : public Component {
 public:
    /**
     * Creates component will NULL member variables
     */
    SoundComp() : mChannel(NULL), mSound(NULL) {}
    /**
     * Releases sound if it exists
     */
    ~SoundComp() {
        if (mSound)
            mSound->release();
    }
    /**
     * Channel of the sound
     */
    FMOD::Channel *mChannel;
    /**
     * FMOD sound component
     */
    FMOD::Sound *mSound;
};

typedef Map<SoundComp> SoundList;

#endif /* SOUNDCOMP_HPP_ */
