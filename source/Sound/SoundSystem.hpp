/*
 * SoundSystem.hpp
 *
 *  Created on: Mar 5, 2014
 *      Author: nikpmup
 */

#ifndef SOUNDSYSTEM_HPP_
#define SOUNDSYSTEM_HPP_

// GL Libraries
#include <GL/glew.h>
// Sound Libraries
#include <fmod/fmod.hpp>
// Cupcake Libraries
#include <Cupcake/Base/System.hpp>
#include <Cupcake/BaseComponent/TransformComp.hpp>
#include <Cupcake/Entity/EntityManager.hpp>
#include <Cupcake/Sound/SoundComp.hpp>

/**
 * System managing sounds and their properties
 */
class SoundSystem : public System {

 public:
    /**
     * Creates SoundSystem with reference to a transformation list
     * Note : init() is still required
     * @param transf
     * @see init
     */
    SoundSystem(TransfList &transf);
    /**
     * Plays sounds if available and update sound locations
     */
    GLvoid update();
    /**
     * Initializes FMOD Library
     */
    GLvoid init();
    /**
     * Removes all sounds and removes FMOD library
     */
    GLvoid release();
    /**
     * Adds sound component to the system
     * @param id : ID associated to sound component
     * @param soundFile : Sound file location
     */
    GLvoid addSound(Entity &id, const GLchar *soundFile);
    /**
     * Adds stream to the system
     * @param id : ID associated to sound component
     * @param soundFile : Sound file location
     */
    GLvoid addStream(Entity &id, const GLchar *soundFile);
    /**
     * Plays sound on the system
     * @param id : ID associated to sound component
     * @param mode : Mode to play the sound as
     * @param volume : Volume to play sound
     */
    GLvoid playSound(Entity &id, const FMOD_MODE &mode, const GLfloat &min,
                     const GLfloat &max, const GLfloat &volume);
    /**
     * Adds listener to system
     * @param id : Listener to add
     */
    GLvoid addListener(const Entity &id);
    /**
     * List of transformations
     */
    TransfList &mTransfList;
 private:
    /**
     * Listener ID : TODO : add list for multiple listeners
     */
    Entity mListenerID;
    /**
     * FMOD System for creating and playing sounds
     */
    FMOD::System *mSystem;
    /**
     * List contianing sound components
     */
    SoundList mSoundList;
    /**
     * Initializes FMOD System
     */
    GLvoid initSystem();
    /**
     * Updates the given sound to the position provided
     * @param sound : Sound to update
     * @param pos : Position of the sound
     */
    GLvoid updatePosition(const Entity &id);
    /**
     * Updates listener
     * @param id : ID of listener
     */
    GLvoid updateListener(const Entity &id);
};

#endif /* SOUNDSYSTEM_HPP_ */
