/*
 * ProgramComp.hpp
 *
 *  Created on: Feb 21, 2014
 *      Author: nikpmup
 */

#ifndef PROGRAMCOMP_HPP_
#define PROGRAMCOMP_HPP_

// GL Libraries
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Contains program ID
 */
class ProgramComp : public Component {
 public:
    /**
     * Initializes component with program ID
     */
    ProgramComp(const GLuint progID) : mProgID(progID) {}
    /**
     * Release program component
     */
    ~ProgramComp() {
        glDeleteProgram(mProgID);
    }
    /**
     * Program ID
     */
    GLuint mProgID;
};

typedef Map<ProgramComp> ProgramList;

#endif /* PROGRAMCOMP_HPP_ */
