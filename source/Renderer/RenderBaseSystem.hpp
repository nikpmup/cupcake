/*
 * RenderBaseSystem.hpp
 *
 *  Created on: Feb 22, 2014
 *      Author: nikpmup
 */

#ifndef RENDERBASESYSTEM_HPP_
#define RENDERBASESYSTEM_HPP_

// Cupcake Libraries
#include <Cupcake/Base/System.hpp>
#include <Cupcake/Base/Map.hpp>
#include <Cupcake/Model/MeshComp.hpp>
#include <Cupcake/Model/TextureComp.hpp>
#include <Cupcake/Renderer/ProgramComp.hpp>
#include <Cupcake/Renderer/RenderData.hpp>

/**
 * Parent class of render systems holding shared functionality
 */
class RenderBaseSystem : public System {
 public:
    /**
     * Performs any calculations require before drawing
     */
    virtual GLvoid update() = 0;
    /**
     * Initializes RenderBaseSystem
     */
    virtual GLvoid init() = 0;
    /**
     * Removes and frees all components and system data
     */
    virtual GLvoid release() {
        mMeshList.releaseAll();
        mProgList.releaseAll();
        mTexList.releaseAll();
        glDeleteBuffers(DATA_CNT, mData);
    }
    /**
     * List of mesh components
     */
    MeshList mMeshList;
    /**
     * List of texture components
     */
    TextureList mTexList;
 protected:
    /**
     * List of program components
     */
    ProgramList mProgList;
    /**
     * GLSL handle for vector data
     * @see eVEC
     */
    GLuint mVec[VEC_CNT];
    /**
     * GLSL handle for data
     * @see eDATA
     */
    GLuint mData[DATA_CNT];
    /**
     * Binds mesh data to data handlers
     * @param meshID : Mesh component ID
     */
    GLvoid bindData(const GLuint meshID);
    /**
     * Binds mesh data to data handlers
     * @param mesh : Mesh component to bind
     */
    GLvoid bindData(MeshComp *mesh);
    /**
     * Sets position vector data
     * @see bindData
     */
    GLvoid setPosData();
    /**
     * Sets normal vector data
     * @see bindData
     */
    GLvoid setNormData();
    /**
     * Sets uv vector data
     * @see bindData
     */
    GLvoid setUVData();
    /*
     * Draws currently bound data to screen
     * @param mesh : Mesh component to draw
     * @see bindData
     */
    GLvoid draw(MeshComp *mesh);
};


#endif /* RENDERBASESYSTEM_HPP_ */
