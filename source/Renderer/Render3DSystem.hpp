/*
 * Render3DSystem.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef RENDER3DSYSTEM_HPP_
#define RENDER3DSYSTEM_HPP_

// Cupcake Libraries
#include <Cupcake/BaseComponent/TransformComp.hpp>
#include <Cupcake/BaseComponent/ScaleComp.hpp>
#include <Cupcake/Camera/CameraComp.hpp>
#include <Cupcake/Entity/EntityManager.hpp>
#include <Cupcake/Light/LightComp.hpp>
#include <Cupcake/Model/MaterialComp.hpp>
#include <Cupcake/Model/MeshComp.hpp>
#include <Cupcake/Model/TextureComp.hpp>
#include <Cupcake/Renderer/RenderBaseSystem.hpp>

/**
 * System that renderers 3D objects with phong shading and textures
 */
class Render3DSystem : public RenderBaseSystem {
 public:
    /**
     * Initializes system with outside reference to a list of transformation
     * @param transList : Transformation component list
     * @see TransformComp
     * @param scaleList : Scale component list
     * @see ScaleComp
     */
    Render3DSystem(TransfList &transList, ScaleList &scaleList);
    /**
     * Renders all 3D entities to the screen
     */
    GLvoid update();
    /**
     * Initializes Render3DSystem
     */
    GLvoid init();
    /**
     * Removes and frees all components and system data
     */
    GLvoid release();
    /**
     * Adds a renderable object to the renderer
     * @param entityID : ID associated with entity
     * @param prog : Program to draw entity with
     * @param mesh : Reference to entity mesh
     * @param mat : Reference to entity material
     * @param tex : Reference to entity texture
     * @param cam : Reference to entity camera
     * @param light : Reference to entity light
     */
    GLvoid addRenderable(Entity entity, const GLuint prog, const GLuint mesh,
                         const GLuint mat, const GLuint tex, const GLuint cam,
                         const GLuint light);
    /**
     * Prevents entity from being rendered but does not remove it's components
     * @param entity : Entity to disable
     */
    GLvoid disableRenderable(Entity entity);
    /**
     * Enables entity to be rendered
     * @param entity : Entity to enable
     */
    GLvoid enableRenderable(Entity entity);
    /**
     * Removes the entity's renderable components
     * @param entity : Entity to remove
     */
    GLvoid removeRenderable(Entity entity);
    /**
     * Installs and saves program to system
     * @param progID : Program ID
     * @param vert : Path to vertex shader
     * @param frag : Path to fragment shader
     */
    GLvoid installProgram(const GLuint progID, const GLchar *vert,
                          const GLchar *frag);
    /**
     * List of transformation components
     */
    TransfList &mTransfList;
    /**
     * List of scale components
     */
    ScaleList &mScaleList;
    /**
     * List of material components
     */
    MaterialList mMatList;
    /**
     * List of camera components
     */
    CameraList mCamList;
    /**
     * List of light components
     */
    LightList mLightList;
 private:
    /**
     * Structure for matching entity ID to component ID's
     */
    typedef struct Renderable {
        Renderable()
                : shouldRender(false),
                  prog(0),
                  mesh(0),
                  mat(0),
                  tex(0),
                  cam(0),
                  light(0) {
        }
        Renderable(const GLboolean _should, const GLuint _prog,
                   const GLuint _mesh, const GLuint _mat, const GLuint _tex,
                   const GLuint _cam, const GLuint _light)
                : shouldRender(_should),
                  prog(_prog),
                  mesh(_mesh),
                  mat(_mat),
                  tex(_tex),
                  cam(_cam),
                  light(_light) {
        }
        GLboolean shouldRender;
        GLuint prog;
        GLuint mesh;
        GLuint mat;
        GLuint tex;
        GLuint cam;
        GLuint light;
    } Renderable;
    /**
     * Typedef for renderable list
     */
    typedef std::map<Entity, Renderable> RenderMap;
    /**
     * Typedef for renderable iter
     */
    typedef RenderMap::iterator RenderIter;
    /**
     * Map containing a link between entity and renderables
     */
    RenderMap mRenderableList;
    /**
     * GLSL handle for matrix data
     * @see eMTX
     */
    GLuint mMtx[MTX_CNT];
    /**
     * GLSL handle for light data
     * @see eLIGHT
     */
    GLuint mLight[LT_CNT];
    /**
     * GLSL handle for material data
     */
    GLuint mMat[MAT_CNT];
    /**
     * GLSL handle for misc data
     * @see eMISC
     */
    GLuint mMisc[MISC_CNT];
    /**
     * Binds GLSL handles to GLSL program
     * @param progID : Program component ID
     */
    GLvoid bindID(ProgramComp *prog);
    /**
     * Draw entity to the screen
     * @param entity : entity to draw
     */
    GLvoid drawEntity(const Entity entityID, Renderable &renderable);
    /**
     * Sends model matrix and normal matrix
     * @param trans : Transformation component
     * @param cam : Camera component
     */
    GLvoid setModelData(TransformComp *trans, ScaleComp *scale,
                        CameraComp *cam);
    /**
     * Sends camera pos, projec matrix, and view matrix
     * @param cam : Camera component
     * @see CameraComp
     */
    GLvoid setCameraData(CameraComp *cam);
    /**
     * Sends light pos, color, and intensity
     * @param light : Light component
     * @see LightComp
     */
    GLvoid setLightData(LightComp *light);
    /**
     * Sends material ambient, diffuse, specular, and reflective data
     * @param mat : Material component
     * @see MaterialComp
     */
    GLvoid setMatData(MaterialComp *mat);
    /**
     * Sends texture id
     * @param tex : Texture component
     * @see TextureComp
     */
    GLvoid setTexData(TextureComp *tex);
};

#endif /* RENDER3DSYSTEM_HPP_ */
