/*
 * Render3DSystem.cpp
 *
 *  Created on: Feb 20, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Renderer/Render3DSystem.hpp>
// Math Libraries
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
// Cupcake Libraries
#include <Cupcake/Util/ShaderLoader.hpp>

//----------------------------------------------------------------------------
//
Render3DSystem::Render3DSystem(TransfList &transList, ScaleList &scaleList)
        : mTransfList(transList),
          mScaleList(scaleList) {
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::update() {
    GLuint curProg = UINT_MAX;
    GLuint curMesh = UINT_MAX;
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);

    Renderable *renderable;
    for (RenderIter iter = mRenderableList.begin();
            iter != mRenderableList.end(); ++iter) {
        renderable = &iter->second;

        if (renderable->shouldRender) {
            if (curProg != renderable->prog) {
                curProg = renderable->prog;
                glUseProgram(mProgList.getEntry(curProg)->mProgID);
            }
            if (curMesh != renderable->mesh) {
                curMesh = renderable->mesh;
                bindData(curMesh);
            }
            drawEntity(iter->first, *renderable);
        }
    }

    glDisable(GL_TEXTURE_2D);
    glActiveTexture(0);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::init() {
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::release() {
    RenderBaseSystem::release();
    mMatList.releaseAll();
    mCamList.releaseAll();
    mLightList.releaseAll();
    mTexList.releaseAll();
    mProgList.releaseAll();
    mRenderableList.clear();
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::addRenderable(Entity entity, const GLuint prog,
                                     const GLuint mesh, const GLuint mat,
                                     const GLuint tex, const GLuint cam,
                                     const GLuint light) {
    mRenderableList[entity] = Renderable(true, prog, mesh, mat, tex, cam,
                                         light);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::disableRenderable(Entity entity) {
    mRenderableList[entity].shouldRender = false;
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::enableRenderable(Entity entity) {
    mRenderableList[entity].shouldRender = true;
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::removeRenderable(Entity entity) {
    mRenderableList.erase(entity);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::bindID(ProgramComp *prog) {
    const GLuint progID = prog->mProgID;
    // Binds program
    glUseProgram(progID);
    // Generate data buff
    glGenBuffers(DATA_CNT, mData);

    // Bind Vector id
    mVec[VEC_POS] = glGetAttribLocation(progID, vecName[VEC_POS]);
    mVec[VEC_NORM] = glGetAttribLocation(progID, vecName[VEC_NORM]);
    mVec[VEC_UV] = glGetAttribLocation(progID, vecName[VEC_UV]);
    // Bind Matrix id
    mMtx[MTX_MODEL] = glGetUniformLocation(progID, mtxName[MTX_MODEL]);
    mMtx[MTX_VIEW] = glGetUniformLocation(progID, mtxName[MTX_VIEW]);
    mMtx[MTX_PROJ] = glGetUniformLocation(progID, mtxName[MTX_PROJ]);
    mMtx[MTX_NORM] = glGetUniformLocation(progID, mtxName[MTX_NORM]);
    // Bind LightComp id
    mLight[LT_POS] = glGetUniformLocation(progID, lightName[LT_POS]);
    mLight[LT_COLOR] = glGetUniformLocation(progID, lightName[LT_COLOR]);
    mLight[LT_INT] = glGetUniformLocation(progID, lightName[LT_INT]);
    // Bind MaterialComp id
    mMat[MAT_AMB] = glGetUniformLocation(progID, matName[MAT_AMB]);
    mMat[MAT_DIFF] = glGetUniformLocation(progID, matName[MAT_DIFF]);
    mMat[MAT_SPEC] = glGetUniformLocation(progID, matName[MAT_SPEC]);
    mMat[MAT_REFL] = glGetUniformLocation(progID, matName[MAT_REFL]);
    // CameraComp data
    mMisc[MISC_CAM_POS] = glGetUniformLocation(progID, miscName[MISC_CAM_POS]);
    mMisc[MISC_TEX_ID] = glGetUniformLocation(progID, miscName[MISC_TEX_ID]);
    // Unbinds program
    glUseProgram(0);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::drawEntity(const Entity entityID,
                                  Renderable &renderable) {
    TransformComp *trans = mTransfList.getEntry(entityID);
    ScaleComp *scale = mScaleList.getEntry(entityID);
    CameraComp *cam = mCamList.getEntry(renderable.cam);
    LightComp *light = mLightList.getEntry(renderable.light);
    MaterialComp *mat = mMatList.getEntry(renderable.mat);
    TextureComp *tex = mTexList.getEntry(renderable.tex);
    MeshComp *mesh = mMeshList.getEntry(renderable.mesh);

    // Sets model data
    setModelData(trans, scale, cam);
    // Sets camera data
    setCameraData(cam);
    // Sets light data
    setLightData(light);
    // Sets material data
    setMatData(mat);
    // Sets texture data
    setTexData(tex);
    // Draws Mesh
    draw(mesh);
    // Unbinds texture
    glBindTexture(GL_TEXTURE_2D, 0);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::setModelData(TransformComp *trans, ScaleComp *scale,
                                    CameraComp *cam) {
    const glm::mat4 modelMat = trans->getGlmMatrix()
            * glm::scale(scale->getGlmVec());
    const glm::mat4 normMat = glm::inverse(glm::transpose(cam->getViewMat()));

    glUniformMatrix4fv(mMtx[MTX_MODEL], 1, GL_FALSE, glm::value_ptr(modelMat));
    glUniformMatrix4fv(mMtx[MTX_NORM], 1, GL_FALSE, glm::value_ptr(normMat));
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::setCameraData(CameraComp *cam) {
    glUniformMatrix4fv(mMtx[MTX_VIEW], 1, GL_FALSE,
                       glm::value_ptr(cam->getViewMat()));
    glUniformMatrix4fv(mMtx[MTX_PROJ], 1, GL_FALSE,
                       glm::value_ptr(cam->getProjMat()));
    glUniform3fv(mMisc[MISC_CAM_POS], 1, glm::value_ptr(cam->getPos()));
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::setLightData(LightComp *light) {
    glUniform3fv(mLight[LT_POS], 1, glm::value_ptr(light->getPos()));
    glUniform3fv(mLight[LT_COLOR], 1, glm::value_ptr(light->getColor()));
    glUniform1f(mLight[LT_INT], light->getIntensity());
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::setMatData(MaterialComp *mat) {
    glUniform3fv(mMat[MAT_AMB], 1, glm::value_ptr(mat->mAmbient));
    glUniform3fv(mMat[MAT_DIFF], 1, glm::value_ptr(mat->mDiffuse));
    glUniform3fv(mMat[MAT_SPEC], 1, glm::value_ptr(mat->mSpecular));
    glUniform1f(mMat[MAT_REFL], mat->mReflect);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::setTexData(TextureComp *tex) {
    glBindTexture(GL_TEXTURE_2D, tex->mTexID);
    glUniform1i(mMisc[MISC_TEX_ID], 0);
}

//----------------------------------------------------------------------------
//
GLvoid Render3DSystem::installProgram(const GLuint progID, const GLchar *vert,
                                      const GLchar *frag) {
    ProgramComp *prog = ShaderLoader::loadShaders(vert, frag);
    mProgList.addEntry(progID, prog);
    bindID(prog);
}
