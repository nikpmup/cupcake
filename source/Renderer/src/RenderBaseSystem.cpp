/*
 * RenderBaseSystem.cpp
 *
 *  Created on: Feb 27, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Renderer/RenderBaseSystem.hpp>

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::bindData(const GLuint meshID) {
    MeshComp *mesh = mMeshList.getEntry(meshID);

    bindData(mesh);
}

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::bindData(MeshComp *mesh) {
    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_POS]);
    glBufferData(GL_ARRAY_BUFFER, mesh->getPosSize(), mesh->getPosData(),
    GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_NORM]);
    glBufferData(GL_ARRAY_BUFFER, mesh->getNormSize(), mesh->getNormData(),
    GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_UV]);
    glBufferData(GL_ARRAY_BUFFER, mesh->getUVSize(), mesh->getUVData(),
    GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mData[DATA_IDX]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->getIdxSize(),
                 mesh->getIdxData(), GL_STATIC_DRAW);
}

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::setPosData() {
    glEnableVertexAttribArray(mVec[VEC_POS]);
    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_POS]);
    glVertexAttribPointer(mVec[VEC_POS], 3, GL_FLOAT, GL_FALSE, 0, 0);
}

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::setNormData() {
    glEnableVertexAttribArray(mVec[VEC_NORM]);
    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_NORM]);
    glVertexAttribPointer(mVec[VEC_NORM], 3, GL_FLOAT, GL_FALSE, 0, 0);
}

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::setUVData() {
    glEnableVertexAttribArray(mVec[VEC_UV]);
    glBindBuffer(GL_ARRAY_BUFFER, mData[DATA_UV]);
    glVertexAttribPointer(mVec[VEC_UV], 2, GL_FLOAT, GL_FALSE, 0, 0);
}

//----------------------------------------------------------------------------
//
GLvoid RenderBaseSystem::draw(MeshComp *mesh) {
    // Sets vector data
    setPosData();
    setNormData();
    setUVData();
    // Sets idx up
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mData[DATA_IDX]);
    // Draws entity
    glDrawElements(GL_TRIANGLES, mesh->getIdxCount(), GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(mVec[VEC_POS]);
    glDisableVertexAttribArray(mVec[VEC_NORM]);
    glDisableVertexAttribArray(mVec[VEC_UV]);
}
