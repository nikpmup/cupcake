/*
 * Render2DSystem.cpp
 *
 *  Created on: Mar 12, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Renderer/Render2DSystem.hpp>
// Math Libraries
#include <glm/gtc/type_ptr.hpp>
// Cupcake Libraries
#include <Cupcake/Debug/Debug.hpp>
#include <Cupcake/Math/TriMath.hpp>
#include <Cupcake/Text/AtlasHelper.hpp>
#include <Cupcake/Text/TextHelper.hpp>
#include <Cupcake/Util/ShaderLoader.hpp>

//----------------------------------------------------------------------------
//
Render2DSystem::Render2DSystem(const GLuint width, const GLuint height)
        : mFTLib(NULL),
          mWidth((GLfloat) width),
          mHeight((GLfloat) height),
          mColor(0),
          mTexID(0) {
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::update() {
    GLuint curProg = UINT_MAX;
    // Setup OpenGL settings
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);

    Text *text;
    for (TextIter iter = mTextList.begin(); iter != mTextList.end(); ++iter) {
        text = &iter->second;

        if (text->shouldDraw) {
            if (curProg != text->prog) {
                curProg = text->prog;
                glUseProgram(mProgList.getEntry(curProg)->mProgID);
            }

            bindData(iter->first);

            drawText(iter->first, *text);
        }
    }

    glDisable(GL_TEXTURE_2D);
    glActiveTexture(0);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::init() {
    initLibrary();
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::initLibrary() {
    if (FT_Init_FreeType(&mFTLib)) {
        printError("Could not load FT Library\n");
        exit(EXIT_FAILURE);
    }
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::release() {
    FT_Done_FreeType(mFTLib);
    mMeshList.releaseAll();
    mProgList.releaseAll();
    mTexList.releaseAll();
    mAtlasList.releaseAll();
    mTextList.clear();
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::createAtlas(const GLuint id, const GLchar* fontPath,
                                   const GLuint fontSize) {
    FT_Face face;

    if (FT_New_Face(mFTLib, fontPath, 0, &face)) {
        printError("Unable to open font: %s\n", fontPath);
        exit(EXIT_FAILURE);
    }
    FT_Set_Pixel_Sizes(face, 0, fontSize);

    mAtlasList.addEntry(id, AtlasHelper::createAtlas(face));

    FT_Done_Face(face);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::createText(Entity entity, const GLuint progID,
                                  const GLuint atlasID, const GLchar *text,
                                  glm::vec2 pos, const glm::vec3 &color) {
    AtlasComp *atlas = mAtlasList.getEntry(atlasID);
    const GLfloat xZero = -mWidth / 2.0f;
    const GLfloat yZero = mHeight / 2.0f;
    const glm::vec2 SCALE(2.0f / mWidth, 2.0f / mHeight);

    //// Adjust coordinates
    pos.x = xZero + pos.x;
    pos.y = yZero - pos.y;
    pos *= SCALE;

    mMeshList.addEntry(entity, TextHelper::createText(atlas, text, pos, SCALE));
    mTextList[entity] = Text(true, progID, atlasID, color);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::installProgram(const GLuint progID, const GLchar *vert,
                                      const GLchar *frag) {
    ProgramComp *prog = ShaderLoader::loadShaders(vert, frag);
    mProgList.addEntry(progID, prog);
    bindID(prog);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::bindID(ProgramComp *prog) {
    const GLuint progID = prog->mProgID;
    // Binds program
    glUseProgram(progID);
    // Generate data buff
    glGenBuffers(DATA_CNT, mData);

    // Bind Vector id
    mVec[VEC_POS] = glGetAttribLocation(progID, vecName[VEC_POS]);
    mVec[VEC_NORM] = glGetAttribLocation(progID, vecName[VEC_NORM]);
    mVec[VEC_UV] = glGetAttribLocation(progID, vecName[VEC_UV]);
    // Binds color
    mColor = glGetUniformLocation(progID, matName[MAT_DIFF]);
    // Binds texture
    mTexID = glGetUniformLocation(progID, miscName[MISC_TEX_ID]);
    // Unbinds program
    glUseProgram(0);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::drawText(const Entity entityID, Text &text) {
    MeshComp *mesh = mMeshList.getEntry(entityID);

    // Binds text data
    setTextData(text);
    // Draws text
    draw(mesh);
    // Unbinds texture
    glBindTexture(GL_TEXTURE_2D, 0);
}

//----------------------------------------------------------------------------
//
GLvoid Render2DSystem::setTextData(Text &text) {
    AtlasComp *atlas = mAtlasList.getEntry(text.atlas);

    // Binds texture
    glBindTexture(GL_TEXTURE_2D, atlas->mTexId);
    glUniform1i(mTexID, 0);
    // Binds color
    glUniform3fv(mColor, 1, glm::value_ptr(text.color));
}
