/*
 * RenderData.hpp
 *
 *  Created on: Feb 20, 2014
 *      Author: nikpmup
 */

#ifndef RENDERDATA_HPP_
#define RENDERDATA_HPP_

// STD Libraries
#include <vector>

/**
 * GLSL identifiers for vectors
 */
enum eVEC {
    VEC_POS,
    VEC_NORM,
    VEC_UV,
    VEC_CNT
};

/**
 * GLSL identifiers for matrix
 */
enum eMTX {
    MTX_MODEL,
    MTX_VIEW,
    MTX_PROJ,
    MTX_NORM,
    MTX_CNT
};

/**
 * GLSL identifiers for data
 */
enum eDATA {
    DATA_POS,
    DATA_NORM,
    DATA_UV,
    DATA_IDX,
    DATA_CNT
};

/**
 * GLSL identifiers for material
 */
enum eMAT {
    MAT_AMB,
    MAT_DIFF,
    MAT_SPEC,
    MAT_REFL,
    MAT_CNT
};

/**
 * GLSL identifiers for light
 */
enum eLIGHT {
    LT_POS,
    LT_COLOR,
    LT_INT,
    LT_CNT
};

/**
 * GLSL idnetifiers for misc
 */
enum eMISC {
    MISC_CAM_POS,
    MISC_TEX_ID,
    MISC_CNT
};

typedef std::vector<const GLchar*> NameVec;

/**
 * Mapping vector identifiers to name
 */
static NameVec vecName = {
        { "aPosition" },
        { "aNormal" },
        { "aTexCoord" }
};

/**
 * Mapping matrix identifiers to name
 */
static NameVec mtxName = {
        { "uModelMat" },
        { "uViewMat" },
        { "uProjMat" },
        { "uNormMat" }
};

/**
 * Mapping light identifiers to name
 */
static NameVec lightName = {
        { "uLightPos" },
        { "uLightColor" },
        { "uLightIntensity" }
};

/**
 * Mapping material identifiers to name
 */
static NameVec matName = {
        { "uAmbient" },
        { "uDiffuse" },
        { "uSpecular" },
        { "uReflect" }
};

/**
 * Mapping misc identifiers to name
 */
static NameVec miscName = {
        { "uCamPos" },
        { "uTexId" }
};

#endif /* RENDERDATA_HPP_ */
