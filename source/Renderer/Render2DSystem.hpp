/*
 * Render2DSystem.hpp
 *
 *  Created on: Feb 26, 2014
 *      Author: nikpmup
 */

#ifndef RENDER2DSYSTEM_HPP_
#define RENDER2DSYSTEM_HPP_

// GL Libraries
#include <GL/glew.h>
// Math Libraries
#include <glm/glm.hpp>
// Font Libraries
#include <ft2build.h>
#include FT_FREETYPE_H
// Cupcake Libraries
#include <Cupcake/Entity/EntityManager.hpp>
#include <Cupcake/Renderer/RenderBaseSystem.hpp>
#include <Cupcake/Renderer/RenderData.hpp>
#include <Cupcake/Text/AtlasComp.hpp>

/**
 * Renders 2D objects such as font or UI
 */
class Render2DSystem : public RenderBaseSystem {

 public:
    /**
     * Initializes system with the window size used in 2D texture creation
     * @param width : Width of the window
     * @param height : Height of the window
     */
    Render2DSystem(const GLuint width, const GLuint height);
    /**
     * Renders all 2D entities to the screen
     */
    GLvoid update();
    /**
     * Initializes font library
     */
    GLvoid init();
    /**
     * Releases all components and font library
     */
    GLvoid release();
    /**
     * Creates an atlas component at the provided id
     * @param id : ID of the atlas
     * @param fontPath : Font location for atlas
     * @param fontSize : Font Size of atlas
     */
    GLvoid createAtlas(const GLuint id, const GLchar* fontPath,
                       const GLuint fontSize);
    /**
     * Creates text at the provided id using the provided atlas.
     * @param entity : Entity text is associated with
     * @param progID : ID of GLSL program to use
     * @param atlasID : ID of atlas to use
     * @param text : Text used
     * @param pos : Position of text
     * @param color : Color of text
     */
    GLvoid createText(Entity entity, const GLuint progID, const GLuint atlasID,
                      const GLchar *text, glm::vec2 pos,
                      const glm::vec3 &color);
    /**
     * Installs and saves program to system
     * @param progID : Program ID
     * @param vert : Path to vertex shader
     * @param frag : Path to fragment shader
     */
    GLvoid installProgram(const GLuint progID, const GLchar *vert,
                          const GLchar *frag);
 private:
    typedef struct Text {
        Text()
                : shouldDraw(false),
                  prog(0),
                  atlas(0),
                  color(glm::vec3(0.0f)) {
        }
        Text(const GLboolean _should, const GLuint _prog, const GLuint _atlas,
             const glm::vec3 &_color)
                : shouldDraw(_should),
                  prog(_prog),
                  atlas(_atlas),
                  color(_color) {
        }
        GLboolean shouldDraw;
        GLuint prog;
        GLuint atlas;
        glm::vec3 color;
    } Text;
    /**
     * Typedef for text list
     */
    typedef std::map<Entity, Text> TextList;
    /**
     * Typedef for text iterator
     */
    typedef TextList::iterator TextIter;
    /**
     * List of text
     */
    TextList mTextList;
    /**
     * List of atlas components
     */
    AtlasList mAtlasList;
    /**
     * Library creating freetype objects
     */
    FT_Library mFTLib;
    /**
     * Width of the window
     */
    GLfloat mWidth;
    /**
     * Height of the window
     */
    GLfloat mHeight;
    /**
     * GLSL handle for color
     */
    GLuint mColor;
    /**
     * GLSL handle for texture ID
     */
    GLuint mTexID;
    /**
     * Initializes FT_Library
     */
    GLvoid initLibrary();
    /**
     * Binds GLSL handles to GLSL program
     * @param progID : Program component ID
     */
    GLvoid bindID(ProgramComp *prog);
    /**
     * Draws text onto the screen
     * @param entityID : Text entity to draw
     */
    GLvoid drawText(const Entity entityID, Text &text);
    /**
     * Sends text color and texture ID to shader
     * @param text : Reference to text obj
     */
    GLvoid setTextData(Text &text);

};

#endif /* RENDER2DSYSTEM_HPP_ */
