/*
 * DeviceSystem.cpp
 *
 *  Created on: Feb 24, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Device/DeviceSystem.hpp>
// STD Libraries
#include <cstdlib>
#include <cstdio>
// GL Library
#include <GLFW/glfw3.h>
// Cupcake Library
#include <Cupcake/Debug/Debug.hpp>

//----------------------------------------------------------------------------
//
DeviceSystem::DeviceSystem(const GLuint width, const GLuint height,
                           const GLchar *title, LoopFunc func)
        : mWindow(NULL),
          mLoopFunc(func),
          mWidth(width),
          mHeight(height) {
    initSystem(width, height, title);
}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::initSystem(const GLuint width, const GLuint height,
                                const GLchar *title) {
    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    mWindow = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(),
                               NULL);

    if (!mWindow) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(mWindow);

    glewExperimental = true;
    GLenum err = glewInit();
    if (err != GLEW_OK) {
        fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::errorCallback(GLint error, const GLchar *desc) {
    fprintf(stderr, "GLFW Error: %s\n", desc);
}

//----------------------------------------------------------------------------
//
GLuint DeviceSystem::getWidth() {
    return mWidth;
}

//----------------------------------------------------------------------------
//
GLuint DeviceSystem::getHeight() {
    return mHeight;
}

//----------------------------------------------------------------------------
//
GLfloat DeviceSystem::getRatio() {
    return (GLfloat) mWidth / mHeight;
}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::setInput(GLFWkeyfun keyCallback) {
    glfwSetKeyCallback(mWindow, keyCallback);
}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::setLoopFunc(LoopFunc func) {
    mLoopFunc = func;
}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::runLoop() {
    while (!glfwWindowShouldClose(mWindow)) {
        mLoopFunc();
        glfwSwapBuffers(mWindow);
        glfwPollEvents();
    }
}

//----------------------------------------------------------------------------
//
GLvoid DeviceSystem::release() {
    glfwDestroyWindow(mWindow);
    glfwTerminate();
}
