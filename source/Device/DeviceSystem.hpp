/*
 * DeviceSystem.hpp
 *
 *  Created on: Feb 24, 2014
 *      Author: nikpmup
 */

#ifndef DEVICESYSTEM_HPP_
#define DEVICESYSTEM_HPP_

// GL Libraries
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class DeviceSystem {

 public:
    typedef GLvoid (*LoopFunc)();
    /**
     * Initializes system with provided window and input
     * @param window : Window used for this system
     * @param input : Input used for this system
     */
    DeviceSystem(const GLuint width, const GLuint height, const GLchar *title,
                 LoopFunc func);
    /**
     * Gets current window width
     * @return Window width in unsigned int
     */
    GLuint getWidth();
    /**
     * Gets current window height
     * @return Window height in unsigned int
     */
    GLuint getHeight();
    /**
     * Gets current window width to height ratio
     * @return Window width to height ratio
     */
    GLfloat getRatio();
    /**
     * Sets current input to the provided input
     * @param input : New input to be used
     */
    GLvoid setInput(GLFWkeyfun keyCallback);
    /**
     * Sets current loop functions to the provided function
     * @param func : New function to be used
     */
    GLvoid setLoopFunc(LoopFunc func);
    /**
     * Runs a loop until window is closed calling the loop function
     */
    GLvoid runLoop();
    /**
     * Release window
     */
    GLvoid release();
 private:
    /**
     * GLFW window
     */
    GLFWwindow *mWindow;
    /**
     * Function pointer to the loop callback
     */
    LoopFunc mLoopFunc;
    /**
     * Width of the window
     */
    GLuint mWidth;
    /**
     * Height of the window
     */
    GLuint mHeight;
    /**
     * Initializes GLFW and GLEW for the system with a window using the
     * provided parameters
     * @param width : Width of the window
     * @param height : Height of the window
     * @param title : Title of the window
     */
    GLvoid initSystem(const GLuint width, const GLuint height,
                      const GLchar *title);
    /**
     * Callback for glfw errors
     * @param error : Error value
     * @param desc : Description of error
     */
    static GLvoid errorCallback(GLint error, const GLchar *desc);
};

#endif /* DEVICESYSTEM_HPP_ */
