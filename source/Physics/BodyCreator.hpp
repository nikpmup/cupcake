/*
 * BodyCreator.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef BODYCREATOR_HPP_
#define BODYCREATOR_HPP_

// Cupcake Libraries
#include <Cupcake/Physics/BodyComp.hpp>

class BodyCreator {

 public:
    static BodyComp* createBody(const GLfloat mass, const glm::vec3 &inertia,
                                const glm::vec3 &pos, btCollisionShape *shape);
    static BodyComp* createBody(const GLfloat mass, const glm::vec3 &pos,
                                btCollisionShape *shape);
    static BodyComp* createBody(const GLfloat mass, const glm::vec3 &inertia,
                                const btTransform &trans,
                                btCollisionShape *shape);
    static BodyComp* createBody(const GLfloat mass, const btTransform &trans,
                                btCollisionShape *shape);
};

#endif /* BODYCREATOR_HPP_ */
