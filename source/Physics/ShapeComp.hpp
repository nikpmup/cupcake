/*
 * ShapeComp.hpp
 *
 *  Created on: Feb 18, 2014
 *      Author: nikpmup
 */

#ifndef SHAPECOMP_HPP_
#define SHAPECOMP_HPP_

// GL Libraries
#include <GL/glew.h>
// Physics Library
#include <bullet/btBulletDynamicsCommon.h>
// Math Library
#include <glm/glm.hpp>
// Cupcake Library
#include <Cupcake/Base/Component.hpp>

class ShapeComp : public Component {
 public:
    ShapeComp(btCollisionShape *shape)
            : mShape(shape) {
    }
    ~ShapeComp() {
        delete mShape;
    }
    btCollisionShape *mShape;
};

typedef Map<ShapeComp> ShapeList;

#endif /* SHAPECOMP_HPP_ */
