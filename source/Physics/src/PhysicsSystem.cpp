/*
 * PhysicsSystem.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Physics/PhysicsSystem.hpp>
// STD Libraries
#include <algorithm>
#include <cstdlib>
// Math Libraries
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
// Cupcake Libraries
#include <Cupcake/Util/TypeConverter.hpp>

//----------------------------------------------------------------------------
//
PhysicsSystem::PhysicsSystem(TransfList &transList)
        : mTransfList(transList),
          mBroadphase(NULL),
          mCollisionConfig(NULL),
          mCollisionDispatcher(NULL),
          mSeqImpConstSolv(NULL),
          mDynWorld(NULL),
          mFrameRate(60.0f) {
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::update() {
    mDynWorld->stepSimulation(1 / mFrameRate);
    for (PhysIter iter = mPhysList.begin(); iter != mPhysList.end(); ++iter) {
        if (iter->second == true)
            syncComponents(iter->first);
    }
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::init(const glm::vec3 &gravity, const GLfloat frameRate) {
    init();
    setGravity(gravity);
    mFrameRate = frameRate;
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::init() {
    mBroadphase = new btDbvtBroadphase();
    mCollisionConfig = new btDefaultCollisionConfiguration();
    mCollisionDispatcher = new btCollisionDispatcher(mCollisionConfig);
    mSeqImpConstSolv = new btSequentialImpulseConstraintSolver();
    mDynWorld = new btDiscreteDynamicsWorld(mCollisionDispatcher, mBroadphase,
                                            mSeqImpConstSolv, mCollisionConfig);
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::release() {
    for (PhysIter iter = mPhysList.begin(); iter != mPhysList.end(); ++iter) {
        if (iter->second == true) {
            BodyComp *body = mBodyList.getEntry(iter->first);
            mDynWorld->removeRigidBody(body->mRigidBody);
        }
    }
    delete mDynWorld;
    delete mSeqImpConstSolv;
    delete mCollisionDispatcher;
    delete mCollisionConfig;
    delete mBroadphase;
    mBodyList.releaseAll();
    mShapeList.releaseAll();
    mPhysList.clear();
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::setGravity(const glm::vec3 &gravity) {
    mDynWorld->setGravity(TypeConverter::glmToBT(gravity));
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::addBody(Entity entity, BodyComp *comp) {
    mBodyList.addEntry(entity, comp);
    mPhysList[entity] = true;
    mDynWorld->addRigidBody(comp->mRigidBody);
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::removeBody(Entity entity) {
    PhysIter iter = mPhysList.find(entity);
    // Checks if entry exists
    if (iter != mPhysList.end()) {
        BodyComp *comp = mBodyList.getEntry(entity);
        if (iter->second == true)
            mDynWorld->removeRigidBody(comp->mRigidBody);
        mPhysList.erase(iter);
        mBodyList.releaseEntry(entity);
    }
}

//----------------------------------------------------------------------------
//
GLvoid PhysicsSystem::syncComponents(Entity entity) {
    TransformComp *trans = mTransfList.getEntry(entity);
    BodyComp *body = mBodyList.getEntry(entity);

    body->mMotion->getWorldTransform(trans->mMatrix);
}
