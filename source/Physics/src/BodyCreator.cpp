/*
 * BodyCreator.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Physics/BodyCreator.hpp>
// Cupcake Libraries
#include <Cupcake/Util/TypeConverter.hpp>

//----------------------------------------------------------------------------
//
BodyComp* BodyCreator::createBody(const GLfloat mass, const glm::vec3 &inertia,
                                  const glm::vec3 &pos,
                                  btCollisionShape *shape) {
    btDefaultMotionState *motion = new btDefaultMotionState(
            btTransform(btQuaternion(0.0f, 0.0f, 0.0f, 1.0f),
                        TypeConverter::glmToBT(pos)));
    btRigidBody *body = new btRigidBody(mass, motion, shape,
                                        TypeConverter::glmToBT(inertia));
    return new BodyComp(motion, body);
}

//----------------------------------------------------------------------------
//
BodyComp* BodyCreator::createBody(const GLfloat mass, const glm::vec3 &pos,
                                  btCollisionShape *shape) {
    btVector3 localInertia(0.0f, 0.0f, 0.0f);
    shape->calculateLocalInertia(mass, localInertia);

    btDefaultMotionState *motion = new btDefaultMotionState(
            btTransform(btQuaternion(0.0f, 0.0f, 0.0f, 1.0f),
                        TypeConverter::glmToBT(pos)));
    btRigidBody *body = new btRigidBody(mass, motion, shape, localInertia);

    return new BodyComp(motion, body);
}

//----------------------------------------------------------------------------
//
BodyComp* BodyCreator::createBody(const GLfloat mass, const glm::vec3 &inertia,
                                  const btTransform &trans,
                                  btCollisionShape *shape) {
    btDefaultMotionState *motion = new btDefaultMotionState(trans);
    btRigidBody *body = new btRigidBody(mass, motion, shape,
                                        TypeConverter::glmToBT(inertia));
    return new BodyComp(motion, body);
}

//----------------------------------------------------------------------------
//
BodyComp* BodyCreator::createBody(const GLfloat mass, const btTransform &trans,
                                  btCollisionShape *shape) {
    btVector3 localInertia(0.0f, 0.0f, 0.0f);
    shape->calculateLocalInertia(mass, localInertia);

    btDefaultMotionState *motion = new btDefaultMotionState(trans);
    btRigidBody *body = new btRigidBody(mass, motion, shape, localInertia);

    return new BodyComp(motion, body);
}
