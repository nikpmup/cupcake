/*
 * ShapeCreator.cpp
 *
 *  Created on: Feb 18, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Physics/ShapeCreator.hpp>
// Cupcake Libraries
#include <Cupcake/Util/TypeConverter.hpp>

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createSphere(const GLfloat radius) {
    return new ShapeComp(new btSphereShape(radius));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createBox(const glm::vec3 &dimen) {
    return new ShapeComp(new btBoxShape(TypeConverter::glmToBT(dimen)));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createBox(const GLfloat x, const GLfloat y,
                                   const GLfloat z) {
    return new ShapeComp(new btBoxShape(btVector3(x, y, z)));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createCyclinder(const glm::vec3 &dimen) {
    return new ShapeComp(new btCylinderShape(TypeConverter::glmToBT(dimen)));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createCyclinder(const GLfloat radius,
                                         const GLfloat height) {
    return new ShapeComp(
            new btCylinderShape(btVector3(radius, 0.5f * height, radius)));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createCyclinder(const GLfloat radius,
                                         const GLfloat height,
                                         const GLfloat radius2) {
    return new ShapeComp(
            new btCylinderShape(btVector3(radius, 0.5f * height, radius2)));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createCapsule(const GLfloat radius,
                                       const GLfloat height) {
    return new ShapeComp(new btCapsuleShape(radius, height));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createCone(const GLfloat radius,
                                    const GLfloat height) {
    return new ShapeComp(new btConeShape(radius, height));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createPlane(const glm::vec3 &norm,
                                     const GLfloat dist) {
    return new ShapeComp(
            new btStaticPlaneShape(TypeConverter::glmToBT(norm), dist));
}

//----------------------------------------------------------------------------
//
ShapeComp* ShapeCreator::createPlane(const GLfloat x, const GLfloat y,
                                     const GLfloat z, const GLfloat dist) {
    return new ShapeComp(new btStaticPlaneShape(btVector3(x, y, z), dist));
}
