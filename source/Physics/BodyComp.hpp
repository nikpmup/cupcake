/*
 * BodyComp.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef BODYCOMP_HPP_
#define BODYCOMP_HPP_

// GL Library
#include <GL/glew.h>
// Physic Library
#include <bullet/btBulletDynamicsCommon.h>
// Math Library
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

/**
 * Component containing rigid body information and motion state
 */
class BodyComp : public Component {

 public:
    /**
     * Constructs BodyComp with motion state and rigid body
     */
    BodyComp(btDefaultMotionState *motion, btRigidBody *body)
            : mMotion(motion),
              mRigidBody(body) {
    }
    /**
     * Removes moition state and rigid body from memory
     */
    ~BodyComp() {
        delete mMotion;
        delete mRigidBody;
    }
    /**
     * Pointer to motion state
     */
    btDefaultMotionState *mMotion;
    /**
     * Pointer to rigid body
     */
    btRigidBody *mRigidBody;
};

typedef Map<BodyComp> BodyList;

#endif /* BODYCOMP_HPP_ */
