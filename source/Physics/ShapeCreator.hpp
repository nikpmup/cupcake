/*
 * ShapeCreator.hpp
 *
 *  Created on: Feb 18, 2014
 *      Author: nikpmup
 */

#ifndef SHAPECREATOR_HPP_
#define SHAPECREATOR_HPP_

// GL Libraries
#include <GL/glew.h>
// Physics Library
#include <bullet/btBulletDynamicsCommon.h>
// Math Library
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Physics/ShapeComp.hpp>

class ShapeCreator {
 public:
    static ShapeComp* createSphere(const GLfloat radius);
    static ShapeComp* createBox(const glm::vec3 &dimen);
    static ShapeComp* createBox(const GLfloat x, const GLfloat y,
                                const GLfloat z);
    static ShapeComp* createCyclinder(const glm::vec3 &dimen);
    static ShapeComp* createCyclinder(const GLfloat radius,
                                      const GLfloat height);
    static ShapeComp* createCyclinder(const GLfloat radius,
                                      const GLfloat height,
                                      const GLfloat radius2);
    static ShapeComp* createCapsule(const GLfloat radius, const GLfloat height);
    static ShapeComp* createCone(const GLfloat radius, const GLfloat height);
    static ShapeComp* createPlane(const glm::vec3 &norm, const GLfloat dist);
    static ShapeComp* createPlane(const GLfloat x, const GLfloat y,
                                  const GLfloat z, const GLfloat dist);
};

#endif /* SHAPECREATOR_HPP_ */
