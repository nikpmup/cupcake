/*
 * PhysicsSystem.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef PHYSICSSYSTEM_HPP_
#define PHYSICSSYSTEM_HPP_

// GL Libraries
#include <GL/glew.h>
// Physics Libraries
#include <bullet/btBulletDynamicsCommon.h>
// Math Library
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Base/System.hpp>
#include <Cupcake/Entity/EntityManager.hpp>
#include <Cupcake/BaseComponent/TransformComp.hpp>
#include <Cupcake/Physics/ShapeComp.hpp>
#include <Cupcake/Physics/BodyComp.hpp>

class PhysicsSystem : public System {

 public:
    /**
     * Initializes system with outside reference to lists of position, scale,
     * and rotation.
     * @param trans : Transformation list
     */
    PhysicsSystem(TransfList &trans);
    /**
     * Calculates physics and updates components
     */
    GLvoid update();
    /**
     * Initializes system
     * @param gravity : Sets worlds gravity
     */
    GLvoid init(const glm::vec3 &gravity, const GLfloat frameRate);
    /**
     * Removes and frees all components and system data
     */
    GLvoid release();
    /**
     * Sets gravity on system
     */
    GLvoid setGravity(const glm::vec3 &gravity);
    /**
     * Adds body to system
     * @param entity : Entity id
     * @param comp : Body component
     */
    GLvoid addBody(Entity enity, BodyComp *comp);
    /**
     * Removes body from system
     * @param entity : Entity id
     */
    GLvoid removeBody(Entity entity);
    /**
     * List of transformation components
     */
    TransfList &mTransfList;
    /**
     * List of body components
     */
    BodyList mBodyList;
    /**
     * List of shape components
     */
    ShapeList mShapeList;
 private:
    /**
     * Defines list of valid physics objects
     */
    typedef std::map<Entity, GLboolean> PhysList;
    /**
     * Defines iterator of physics list
     */
    typedef PhysList::iterator PhysIter;
    /**
     * Broadphase Interface
     */
    btBroadphaseInterface *mBroadphase;
    /**
     * Collision Config
     */
    btDefaultCollisionConfiguration *mCollisionConfig;
    /**
     * Collision Dispatcher
     */
    btCollisionDispatcher *mCollisionDispatcher;
    /**
     * Contraint Solver
     */
    btSequentialImpulseConstraintSolver *mSeqImpConstSolv;
    /**
     * Dynamic World
     */
    btDiscreteDynamicsWorld *mDynWorld;
    /**
     * Framerate
     */
    GLfloat mFrameRate;
    /**
     * Keeps track of valid physics objects
     */
    PhysList mPhysList;
    /**
     * Initializes private variables
     */
    GLvoid init();
    /**
     * Updates position component and rotate component from the physics system
     * @param entity : ID to update
     */
    GLvoid syncComponents(Entity entity);
};


#endif /* PHYSICSSYSTEM_HPP_ */
