/*
 * MeshLoader.hpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

#ifndef MESHLOADER_HPP_
#define MESHLOADER_HPP_

// GL Libraries
#include <GL/glew.h>
// Cupcake Header
#include <Cupcake/Model/MeshComp.hpp>

class MeshLoader{
 public:
    static MeshComp* loadObj(const GLchar *path);
};


#endif /* MESHLOADER_HPP_ */
