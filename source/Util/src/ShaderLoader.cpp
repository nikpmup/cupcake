/*
 * ShaderLoader.cpp
 *
 *  Created on: Dec 7, 2013
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Util/ShaderLoader.hpp>
// STD Libraries
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
// Cupcake Header
#include <Cupcake/Debug/Debug.hpp>

//----------------------------------------------------------------------------
//
ProgramComp* ShaderLoader::loadShaders(const GLchar *vert, const GLchar *frag) {
    // Create the shaders
    GLuint vertId = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragId = glCreateShader(GL_FRAGMENT_SHADER);

    std::string vertCode = fileToString(vert);
    std::string fragCode = fileToString(frag);
    const GLchar *vertSrc = vertCode.c_str();
    const GLchar *fragSrc = fragCode.c_str();

    // Compile Vertex Shader
    DBG_CMD(fprintf(stdout, "Compiling shader : %s\n", vert));
    glShaderSource(vertId, 1, &vertSrc, NULL);
    glCompileShader(vertId);
    DBG_CMD(printGLError("glCompileShader"));
    printShaderInfo(vertId);

    // Compile Fragment Shader
    DBG_CMD(fprintf(stdout, "Compiling shader : %s\n", frag));
    glShaderSource(fragId, 1, &fragSrc, NULL);
    glCompileShader(fragId);
    DBG_CMD(printGLError("glCompileShader"));
    printShaderInfo(fragId);

    // Sanity check
    if (!checkCompiled(vertId, fragId)) {
        return 0;
    }

    // Link the program
    GLuint progId = glCreateProgram();
    glAttachShader(progId, vertId);
    glAttachShader(progId, fragId);
    glLinkProgram(progId);
    printProgramInfo(progId);

    glDeleteShader(vertId);
    glDeleteShader(fragId);

    return new ProgramComp(progId);
}

//----------------------------------------------------------------------------
//
std::string ShaderLoader::fileToString(const GLchar *shaderPath) {
    std::string shaderCode;
    std::ifstream shaderStream(shaderPath, std::ios::in);

    if (shaderStream.is_open()) {
        std::string line;
        while (std::getline(shaderStream, line)) {
            shaderCode.append(line + "\n");
        }
        shaderStream.close();
    } else {
        fprintf(stderr, "Could not open %s\n", shaderPath);
    }

    return shaderCode;
}

//----------------------------------------------------------------------------
//
GLvoid ShaderLoader::printShaderInfo(const GLuint shaderId) {
    GLint infoLogLength = 0;
    GLint charWritten = 0;
    GLchar *log;

    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
    DBG_CMD(printGLError("glGetShaderiv"));
    if (infoLogLength > 1) {
        log = new GLchar[infoLogLength];

        if (log) {
            glGetShaderInfoLog(shaderId, infoLogLength, &charWritten, log);
            fprintf(stdout, "Shader info:\n%s\n", log);
        } else {
            fprintf(stderr, "Error: Could not allocate log buffer\n");
            exit(EXIT_FAILURE);
        }

        delete log;
    }
    DBG_CMD(printGLError("printShaderInfo"));
}

//----------------------------------------------------------------------------
//
GLvoid ShaderLoader::printProgramInfo(const GLuint progId) {
    GLint infoLogLength = 0;
    GLint charWritten = 0;
    GLchar *log;

    glGetProgramiv(progId, GL_INFO_LOG_LENGTH, &infoLogLength);
    DBG_CMD(printGLError("glGetProgramiv"));
    if (infoLogLength > 1) {
        log = new GLchar[infoLogLength];

        if (log) {
            glGetProgramInfoLog(progId, infoLogLength, &charWritten, log);
            fprintf(stdout, "Program info:\n%s\n", log);
        } else {
            fprintf(stderr, "Error: Could not allocate log buffer\n");
            exit(EXIT_FAILURE);
        }
        delete log;
    }
    DBG_CMD(printGLError("printProgramInfo"));
}

//----------------------------------------------------------------------------
//
GLboolean ShaderLoader::checkCompiled(const GLuint vertId, const GLuint fragId) {
    GLint vertComp, fragComp;
    GLboolean retVal = true;

    glGetShaderiv(vertId, GL_COMPILE_STATUS, &vertComp);
    glGetShaderiv(fragId, GL_COMPILE_STATUS, &fragComp);

    if (!vertComp || !fragComp) {
        fprintf(stderr, "Error compiling %s",
                vertComp ? "frag shader\n" : "vert shader\n");
        retVal = false;
    }
    return retVal;
}
