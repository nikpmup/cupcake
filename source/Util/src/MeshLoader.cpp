/*
 * MeshLoader.cpp
 *
 *  Created on: Dec 6, 2013
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Util/MeshLoader.hpp>
// STD Libraries
#include <cstdio>
#include <cstdlib>
#include <cstring>
// GL Libraries
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Debug/Debug.hpp>

//----------------------------------------------------------------------------
//
MeshComp* MeshLoader::loadObj(const GLchar *path) {
    MeshComp::vVec3 posVec, normVec;
    MeshComp::vVec2 uvVec;
    MeshComp::vGLuint posIdx, uvIdx, normIdx;

    DBG_CMD(fprintf(stdout, "Loading object file %s\n", path));

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printError("MeshLoader: Unable to open file %s\n", path);
        exit(EXIT_FAILURE);
    }

    char lineHeader[128];
    while (fscanf(file, "%s", lineHeader) != EOF) {
        if (strcmp(lineHeader, "v") == 0) {
            glm::vec3 pos;
            fscanf(file, "%f %f %f\n", &pos.x, &pos.y, &pos.z);
            posVec.push_back(pos);
        } else if (strcmp(lineHeader, "vt") == 0) {
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y);
            uvVec.push_back(uv);
        } else if (strcmp(lineHeader, "vn") == 0) {
            glm::vec3 norm;
            fscanf(file, "%f %f %f\n", &norm.x, &norm.y, &norm.z);
            normVec.push_back(norm);
        } else if (strcmp(lineHeader, "f") == 0) {
            GLuint pos[3], uv[3], norm[3];
            if (fscanf(file, "%u/%u/%u %u/%u/%u %u/%u/%u\n",
                       pos, uv, norm,
                       pos + 1, uv + 1, norm + 1,
                       pos + 2, uv + 2, norm + 2) != 9) {
                printError("MeshLoader: Corrupted index values\n");
                exit(EXIT_FAILURE);
            }
            for (GLuint idx = 0; idx < 3; idx++) {
                posIdx.push_back(pos[idx] - 1);
                uvIdx.push_back(uv[idx] - 1);
                normIdx.push_back(norm[idx] - 1);
            }
        } else {
            char cleanBuffer[1000];
            fgets(cleanBuffer, 1000, file);
        }
    }
    fclose(file);

    MeshComp::vVec3 outPos, outNorm;
    MeshComp::vVec2 outUV;
    MeshComp::vGLuint outIdx;
    for (GLuint idx = 0; idx < posIdx.size(); idx++) {
        GLuint pos = posIdx[idx];
        GLuint uv = uvIdx[idx];
        GLuint norm = normIdx[idx];

        outIdx.push_back(idx);
        outPos.push_back(posVec[pos]);
        outUV.push_back(uvVec[uv]);
        outNorm.push_back(normVec[norm]);
    }

    return new MeshComp(outPos, outUV, outNorm, outIdx);
}
