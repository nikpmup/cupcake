/*
 * TypeConverter.cpp
 *
 *  Created on: Jan 31, 2014
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Util/TypeConverter.hpp>
// GL Libraries
#include <GL/glew.h>

//----------------------------------------------------------------------------
//
FMOD_VECTOR TypeConverter::glmToFMOD(const glm::vec3 &vec) {
    return (FMOD_VECTOR) { vec.x, vec.y, vec.z };
}

//----------------------------------------------------------------------------
//
glm::vec3 TypeConverter::FMODToglm(FMOD_VECTOR &vec) {
    return glm::vec3(vec.x, vec.y, vec.z);
}

//----------------------------------------------------------------------------
//
btVector3 TypeConverter::glmToBT(const glm::vec3 &vec) {
    return btVector3(vec.x, vec.y, vec.z);
}

//----------------------------------------------------------------------------
//
glm::vec3 TypeConverter::BTToglm(const btVector3 &vec) {
    return glm::vec3(vec.getX(), vec.getY(), vec.getZ());
}

//----------------------------------------------------------------------------
//
FMOD_VECTOR TypeConverter::btToFmod(const btVector3 &vec) {
    return (FMOD_VECTOR) { vec.getX(), vec.getY(), vec.getZ() };
}
