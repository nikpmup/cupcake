/*
 * TextureLoader.cpp
 *
 *  Created on: Jan 13, 2014
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Util/TextureLoader.hpp>
// STD Libraries
#include <cstdlib>
#include <cstdio>
// GL Libraries
#include <gli/gli.hpp>

//----------------------------------------------------------------------------
//
TextureComp* TextureLoader::loadDDS(const GLchar *imgPath) {
    gli::texture2D texture(gli::loadStorageDDS(imgPath));
    // Checks if texture opened
    if (texture.empty()) {
        fprintf(stderr, "Unable to open %s\n", imgPath);
        exit(EXIT_FAILURE);
    }

    // Binds texture
    GLuint textureID;

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &textureID);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL,
                    GLint(texture.levels() - 1));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
    GL_NEAREST_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    for (gli::texture2D::size_type Level = 0; Level < texture.levels();
            ++Level) {
        glTexImage2D(
        GL_TEXTURE_2D,
                     GLint(Level),
                     GL_RGBA8,
                     GLsizei(texture[Level].dimensions().x),
                     GLsizei(texture[Level].dimensions().y), 0,
                     GL_BGRA,
                     GL_UNSIGNED_BYTE, texture[Level].data());
    }

    if (texture.levels() == 1)
        glGenerateMipmap(GL_TEXTURE_2D);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

    return new TextureComp(textureID);
}
