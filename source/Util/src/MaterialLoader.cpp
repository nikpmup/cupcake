/*
 * MaterialLoader.cpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Util/MaterialLoader.hpp>
// STD Libraries
#include <cstdio>
#include <cstdlib>
#include <cstring>
// GL Libraries
#include <glm/glm.hpp>
// Cupcake Libraries
#include <Cupcake/Debug/Debug.hpp>

//----------------------------------------------------------------------------
//
MaterialComp* MaterialLoader::loadMaterial(const GLchar *path) {
    glm::vec3 amb, diff, spec;
    GLfloat refl;

    DBG_CMD(fprintf(stdout, "Loading texture file %s\n", path));

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        printError("MaterialLoader: Unable to open file\n");
        exit(EXIT_FAILURE);
    }

    char lineHeader[128];
    while (fscanf(file, "%s", lineHeader) != EOF) {
        if (strcmp(lineHeader, "Ns") == 0) {
            glm::vec3 pos;
            fscanf(file, "%f\n", &refl);
        } else if (strcmp(lineHeader, "Ka") == 0) {
            fscanf(file, "%f %f %f\n", &amb.x, &amb.y, &amb.z);
        } else if (strcmp(lineHeader, "Kd") == 0) {
            fscanf(file, "%f %f %f\n", &diff.x, &diff.y, &diff.z);
        } else if (strcmp(lineHeader, "Ks") == 0) {
            fscanf(file, "%f %f %f\n", &spec.x, &spec.y, &spec.z);
        } else {
            char cleanBuffer[1000];
            fgets(cleanBuffer, 1000, file);
        }
    }
    fclose(file);

    return new MaterialComp(amb, diff, spec, refl);
}
