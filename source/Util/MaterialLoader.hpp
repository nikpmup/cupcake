/*
 * MaterialLoader.hpp
 *
 *  Created on: Feb 25, 2014
 *      Author: nikpmup
 */

#ifndef MATERIALLOADER_HPP_
#define MATERIALLOADER_HPP_

// Cupcake Libraries
#include <Cupcake/Model/MaterialComp.hpp>

class MaterialLoader {
 public:
    static MaterialComp* loadMaterial(const GLchar *path);
};


#endif /* MATERIALLOADER_HPP_ */
