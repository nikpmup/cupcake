/*
 * Singleton.hpp
 *
 *  Created on: Feb 4, 2014
 *      Author: nikpmup
 */

#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

// GL Libraries
#include <GL/glew.h>
// STD Libraries
#include <cassert>

template<typename T>
class Singleton {

 public:
    static T& instance();
    static GLvoid destroy();

 protected:
    inline explicit Singleton() {
        assert(Singleton::mInstance == NULL);
        Singleton::mInstance = static_cast<T*>(this);
    }
    inline ~Singleton() {
        Singleton::mInstance = NULL;
    }

 private:
    static T* mInstance;
    static T* createInstance();
    static GLvoid destroyInstance(T*);
    // Override operators and assignment
    inline explicit Singleton(Singleton const&) {}
    inline Singleton& operator=(Singleton const&) { return *this; }

};

//----------------------------------------------------------------------------
//
template<typename T>
T& Singleton<T>::instance() {
    if (Singleton::mInstance == NULL) {
        Singleton::mInstance = createInstance();
    }
    return *Singleton::mInstance;
}

//----------------------------------------------------------------------------
//
template<typename T>
GLvoid Singleton<T>::destroy() {
    if (Singleton::mInstance != NULL) {
        destroyInstance(Singleton::mInstance);
        Singleton::mInstance = NULL;
    }
}

//----------------------------------------------------------------------------
//
template<typename T>
inline T* Singleton<T>::createInstance() {
    return new T();
}

//----------------------------------------------------------------------------
//
template<typename T>
inline GLvoid Singleton<T>::destroyInstance(T* p) {
    delete p;
}

template<typename T>
T* Singleton<T>::mInstance = NULL;
#endif /* SINGLETON_HPP_ */
