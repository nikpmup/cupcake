/*
 * TypeConverter.hpp
 *
 *  Created on: Jan 31, 2014
 *      Author: nikpmup
 */

#ifndef TYPECONVERTER_HPP_
#define TYPECONVERTER_HPP_

// Math Libraries
#include <glm/glm.hpp>
// Sound Libraries
#include <fmod/fmod.hpp>
// Physics Library
#include <bullet/btBulletDynamicsCommon.h>

class TypeConverter {

 public:
    static FMOD_VECTOR glmToFMOD(const glm::vec3 &vec);
    static glm::vec3 FMODToglm(FMOD_VECTOR &vec);
    static btVector3 glmToBT(const glm::vec3 &vec);
    static glm::vec3 BTToglm(const btVector3 &vec);
    static FMOD_VECTOR btToFmod(const btVector3 &vec);
};

#endif /* TYPECONVERTER_HPP_ */
