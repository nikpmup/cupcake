/*
 * TextureLoader.hpp
 *
 *  Created on: Jan 13, 2014
 *      Author: nikpmup
 */

#ifndef TEXTURELOADER_HPP_
#define TEXTURELOADER_HPP_

// GL Libraries
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Model/TextureComp.hpp>

class TextureLoader {

 public:
    static TextureComp* loadDDS(const GLchar *imgPath);
};


#endif /* TEXTURELOADER_HPP_ */
