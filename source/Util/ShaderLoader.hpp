/*
 * ShaderLoader.hpp
 *
 *  Created on: Dec 7, 2013
 *      Author: nikpmup
 */

#ifndef SHADERLOADER_HPP_
#define SHADERLOADER_HPP_

// STD Libraries
#include <string>
// GL Libraries
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Renderer/ProgramComp.hpp>

class ShaderLoader {
 public:
    static ProgramComp* loadShaders(const GLchar *vert, const GLchar *frag);
 private:
    ShaderLoader();
    static std::string fileToString(const GLchar *shaderPath);
    static GLvoid printShaderInfo(const GLuint shaderId);
    static GLvoid printProgramInfo(const GLuint progId);
    static GLboolean checkCompiled(const GLuint vertId, const GLuint fragId);
};


#endif /* SHADERLOADER_HPP_ */
