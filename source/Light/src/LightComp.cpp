/*
 * LightComp.cpp
 *
 *  Created on: Jan 13, 2014
 *      Author: nikpmup
 */

// Header definition
#include <Cupcake/Light/LightComp.hpp>

//----------------------------------------------------------------------------
//
LightComp::LightComp()
        : mIntensity(0) {
}

//----------------------------------------------------------------------------
//
LightComp::LightComp(const glm::vec3 &lightPos, const glm::vec3 &lightColor,
             const GLfloat lightIntensity)
        : mPos(lightPos),
          mColor(lightColor),
          mIntensity(lightIntensity) {
}

//----------------------------------------------------------------------------
//
GLvoid LightComp::setPos(const glm::vec3 &lightPos) {
    mPos = lightPos;
}

//----------------------------------------------------------------------------
//
glm::vec3 LightComp::getPos() {
    return mPos;
}

//----------------------------------------------------------------------------
//
GLvoid LightComp::setColor(const glm::vec3 &lightColor) {
    mColor = lightColor;
}

//----------------------------------------------------------------------------
//
glm::vec3 LightComp::getColor() {
    return mColor;
}

//----------------------------------------------------------------------------
//
GLvoid LightComp::setIntensity(const GLfloat lightIntensity) {
    mIntensity = lightIntensity;
}

//----------------------------------------------------------------------------
//
GLfloat LightComp::getIntensity() {
    return mIntensity;
}
