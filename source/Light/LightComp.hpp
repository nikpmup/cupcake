/*
 * Light.hpp
 *
 *  Created on: Jan 13, 2014
 *      Author: nikpmup
 */

#ifndef LIGHT_HPP_
#define LIGHT_HPP_

// STD Libraries
#include <vector>
// GL Libraries
#include <glm/glm.hpp>
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Base/Component.hpp>

class LightComp : public Component {

 public:
    // Constructors
    LightComp();
    LightComp(const glm::vec3 &lightPos, const glm::vec3 &lightColor,
              const GLfloat lightIntesity);
    // Position
    GLvoid setPos(const glm::vec3 &lightPos);
    glm::vec3 getPos();
    // Color
    GLvoid setColor(const glm::vec3 &lightColor);
    glm::vec3 getColor();
    // Intensity
    GLvoid setIntensity(const GLfloat lightIntensity);
    GLfloat getIntensity();

 private:
    glm::vec3 mPos;
    glm::vec3 mColor;
    GLfloat mIntensity;
};

typedef Map<LightComp> LightList;

#endif /* LIGHT_HPP_ */
