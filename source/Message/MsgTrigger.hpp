/*
 * MsgTrigger.hpp
 *
 *  Created on: Mar 24, 2014
 *      Author: nikpmup
 */

#ifndef MSGTRIGGER_HPP_
#define MSGTRIGGER_HPP_

// GL Libraries
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Message/MsgSystem.hpp>

class MsgTrigger {

 public:
    MsgTrigger(MsgId msgId) : mMsgId(msgId) {}
    virtual GLboolean shouldGenMsg() { return true; }
    virtual Message genMsg() { return Message(); }
    MsgId getMsgId() { return mMsgId; }
    virtual ~MsgTrigger() {}
 protected:
    MsgId mMsgId;
};

#endif /* MSGTRIGGER_HPP_ */
