/*
 * MsgSystem.hpp
 *
 *  Created on: Mar 19, 2014
 *      Author: nikpmup
 */

#ifndef MSGSYSTEM_HPP_
#define MSGSYSTEM_HPP_

// STD Libraries
#include <vector>
#include <map>
// Cupcake Libraries
#include <Cupcake/Base/System.hpp>
#include <Cupcake/Message/Message.hpp>
#include <Cupcake/Message/MsgHandler.hpp>
#include <Cupcake/Message/MsgTrigger.hpp>

/**
 * System that recieves and passes messages to systems
 */
class MsgSystem : public System {

 public:
    /**
     * Creates Message System
     */
    MsgSystem();
    /**
     * Processes messages into a system
     */
    GLvoid update();
    /**
     * Initializes the system
     */
    GLvoid init();
    /**
     * Releases all messages in the system
     */
    GLvoid release();
    /**
     * Sends messages to the appropriate systems
     * @param msg : Message to handle
     */
    GLvoid handleMessage(const MsgId msgId, Message msg);
    /**
     * Subscribes a script to a message
     * @param msgId : Message to subscribe to
     * @param msgHandle : Script to run message one
     */
    GLvoid subscribeHandler(const MsgId msgId, MsgHandler *msgHandle);
    /**
     * Adds trigger to send messages
     * @param msgTrigger : Trigger to add to the system
     */
    GLvoid addTrigger(MsgTrigger &msgTrigger);
 private:
    /**
     * Type to contain a list of MsgHandlers
     */
    typedef std::vector<MsgHandler*> HandlerList;
    /**
     * Iterator for HandlerList
     */
    typedef HandlerList::iterator HandlerIter;
    /**
     * Type to map MsgId to a list of MsgHandlers
     */
    typedef std::map<MsgId, HandlerList> MsgHandlerMap;
    /**
     * Type to contain a list of MsgTriggers
     */
    typedef std::vector<MsgTrigger> TriggerList;
    /**
     * Iterator for TriggerList
     */
    typedef TriggerList::iterator TriggerIter;
    /**
     * Maps MsgId to MsgHandlers
     */
    MsgHandlerMap mMsgHandlerMap;
    /**
     * List of MsgTriggers
     */
    TriggerList mTriggerList;
};



#endif /* MSGSYSTEM_HPP_ */
