/*

 * MsgSystem.cpp
 *
 *  Created on: Mar 24, 2014
 *      Author: nikpmup
 */

#include <Cupcake/Message/MsgSystem.hpp>
#include <cstdio>

//----------------------------------------------------------------------------
//
MsgSystem::MsgSystem() {
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::update() {
    for (TriggerIter iter = mTriggerList.begin(); iter != mTriggerList.end();
            ++iter) {
        if (iter->shouldGenMsg()) {
            handleMessage(iter->getMsgId(), iter->genMsg());
        }
    }
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::init() {
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::release() {
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::handleMessage(const MsgId msgId, Message msg) {
    HandlerList &handleList = mMsgHandlerMap[msgId];

    for (HandlerIter iter = handleList.begin(); iter != handleList.end();
            ++iter) {
        (*iter)->processMsg(msg);
    }
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::subscribeHandler(const MsgId msgId,
                                       MsgHandler *msgHandle) {
    HandlerList &handleList = mMsgHandlerMap[msgId];

    handleList.push_back(msgHandle);
}

//----------------------------------------------------------------------------
//
GLvoid MsgSystem::addTrigger(MsgTrigger &msgTrigger) {
    mTriggerList.push_back(msgTrigger);
}
