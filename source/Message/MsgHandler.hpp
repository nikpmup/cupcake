/*
 * MsgHandler.hpp
 *
 *  Created on: Mar 19, 2014
 *      Author: nikpmup
 */

#ifndef MSGHANDLER_HPP_
#define MSGHANDLER_HPP_

// GL Libraries
#include <GL/glew.h>
// Cupcake Libraries
#include <Cupcake/Message/Message.hpp>
#include <cstdio>

class MsgHandler {
 public:
    virtual GLvoid processMsg(Message &msg) {
    }
    virtual ~MsgHandler() {}
};

#endif /* MSGHANDLER_HPP_ */
