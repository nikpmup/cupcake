/*
 * EntityManager.hpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

#ifndef ENTITYMANAGER_HPP_
#define ENTITYMANAGER_HPP_

// STD Libraries
#include <vector>
// GL Libraries
#include <GL/glew.h>

typedef GLuint Entity;

/*
 * Manages entity ids and make sure there is no duplicates
 */
class EntityManager {
 public:
    /**
     * Finds the next open spot in the EntityManager. If no space exists,
     * the EntityManagetr will expand the ids.
     * @return Next free id
     */
    Entity getNextId();
    /**
     * Removes the provided id from the EntityManager.
     * @param Id to be removed
     */
    GLvoid removeId(const Entity id);
    /**
     * Removes all id's from the EntityManager
     */
    GLvoid releaseAll();
 private:
    /*
     * List of used ids
     */
    std::vector<GLboolean> mIdList;
};

#endif /* ENTITYMANAGER_HPP_ */
