/*
 * EntityManager.cpp
 *
 *  Created on: Feb 19, 2014
 *      Author: nikpmup
 */

// Header Definition
#include <Cupcake/Entity/EntityManager.hpp>
// STD Libraries
#include <algorithm>

//----------------------------------------------------------------------------
//
Entity EntityManager::getNextId() {
    Entity id;
    std::vector<GLboolean>::iterator iter = std::find(mIdList.begin(),
                                                      mIdList.end(), false);
    if (iter != mIdList.end()) {
        *iter = true;
        id = iter - mIdList.begin();
    } else {
        mIdList.push_back(true);
        id = mIdList.size() - 1;
    }

    return id;
}

//----------------------------------------------------------------------------
//
GLvoid EntityManager::removeId(const Entity id) {
    mIdList[id] = false;
}

//----------------------------------------------------------------------------
//
GLvoid EntityManager::releaseAll() {
    mIdList.clear();
}
